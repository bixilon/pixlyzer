package de.bixilon.pixlyzer.exceptions

class GeneratorSkipException(message: String) : Exception(message)
