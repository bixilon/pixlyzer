package de.bixilon.pixlyzer.generator

abstract class Generator(
    val name: String,
    val allowedFail: Boolean = false,
    val allowEmpty: Boolean = false,
) {
    val entries
        get() = data.size

    val data: MutableMap<Any, Any> = mutableMapOf()

    abstract fun generate()

    open fun test() {}
}
