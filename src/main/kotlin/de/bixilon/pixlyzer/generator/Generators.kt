package de.bixilon.pixlyzer.generator

import de.bixilon.pixlyzer.generator.generators.*
import de.bixilon.pixlyzer.generator.generators.enums.EnchantmentTargetsGenerator
import de.bixilon.pixlyzer.generator.generators.variants.CatVariantGenerator
import de.bixilon.pixlyzer.generator.generators.variants.FrogVariantGenerator


object Generators {
    val GENERATORS: List<Generator> = mutableListOf(
        FeatureGenerator,
        MessageTypeGenerator,
        ArgumentTypeGenerator,
        SoundGroupGenerator,
        CatVariantGenerator,
        FrogVariantGenerator,

        BiomeCategoryGenerator,

        EntityDataDataTypeGenerator,
        MiscGenerator,
        ContainerTypeGenerator,
        EntityGenerator,
        BiomeGenerator,
        MaterialGenerator,
        ItemGenerator,
        StatusEffectGenerator,
        EnchantmentGenerator,
        PotionGenerator,
        MotiveGenerator,
        DimensionGenerator,
        FluidGenerator,
        BlockGenerator,
        RarityGenerator,
        // ToDo: MobCategoryGenerator,
        ParticleGenerator,
        BlockEntityGenerator,
        StatisticsGenerator,
        VersionGenerator,
        VillagerTypeGenerator,
        PointOfInterestGenerator,
        VillagerProfessionGenerator,
        SoundEventGenerator,

        EnchantmentTargetsGenerator,


        VoxelShapeGenerator,
    )
}
