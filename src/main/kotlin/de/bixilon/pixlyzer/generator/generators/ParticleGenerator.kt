package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.particle.ParticleType
import java.io.FileNotFoundException

object ParticleGenerator : Generator(
    "particles"
) {
    val REGISTRY = RegistryUtil.getRegistry<ParticleType<*>>("PARTICLE_TYPE")!!

    override fun generate() {
        for (particleType in ParticleGenerator.REGISTRY) {
            val resourceLocation = ParticleGenerator.REGISTRY.getId(particleType)
            val particleData = compound()
            particleData["id"] = ParticleGenerator.REGISTRY.getRawId(particleType)

            // load render model
            try {
                particleData["render"] = Util.readJsonMinecraftResource("assets/${resourceLocation!!.namespace}/particles/${resourceLocation.path}.json")
            } catch (exception: FileNotFoundException) {
                println("Can not get model: $particleType")
            }

            if (particleType.shouldAlwaysSpawn()) {
                particleData["override_limiter"] = particleType.shouldAlwaysSpawn()
            }
            // ToDo type and meta data

            data[resourceLocation.toString()] = particleData
        }
    }
}
