package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.ReflectionUtil.variable
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util
import de.bixilon.pixlyzer.util.Util.JSON_MAP_TYPE
import de.bixilon.pixlyzer.util.Util.compound
import de.bixilon.pixlyzer.util.Util.mapCast
import de.bixilon.pixlyzer.util.Util.nullCast
import net.minecraft.text.Style
import java.util.*

object MessageTypeGenerator : Generator(
    "message_types",
    allowedFail = true,
    allowEmpty = true,
) {
    private val DEFAULT_FONT = getField(Style::class.java, "DEFAULT_FONT_ID")?.get(null)?.let { Util.MAPPER.convertValue<MutableMap<String, Any>>(it, JSON_MAP_TYPE) }


    val registry = RegistryUtil.getRegistry<Any>("MESSAGE_TYPE")
    val messageTypeClass = getClass("net.minecraft.network.message.MessageType")

    override fun generate() {
        if (registry == null) {
            return
        }

        for (type in registry) {
            val identifier = registry.getId(type)

            val json = compound()

            json["id"] = registry.getRawId(type)
            getField(messageTypeClass, "chat")?.get(type)?.getOptional()?.getDecoration()?.serializeChat()?.let { json["chat"] = it }
            getField(messageTypeClass, "narration")?.get(type)?.getOptional()?.getDecoration()?.serializeDecoration()?.let { json["narration"] = it }

            this.data[identifier.toString()] = json
        }
    }

    fun Any.getOptional(): Any? {
        if (this is Optional<*>) {
            return if (this.isPresent) this.get() else null
        }
        return this
    }

    fun Any.getDecoration(): Any? {
        this.variable<Any>("decoration")?.let { return it.getOptional() }
        return this
    }

    private fun Any.serializeChat(): Map<String, Any?>? {
        if (this::class.java.simpleName == "Decoration") {
            return this.serializeDecoration()
        }
        var value = this.getOptional() ?: return null
        return getField(value::class.java, "decoration")?.get(value)?.nullCast<Optional<Any>>()?.get()?.serializeDecoration()
    }

    fun Any.serializeDecoration(): Map<String, Any?> {
        return mapOf(
            "translation_key" to getField(this::class.java, "translationKey")?.get(this),
            "parameters" to getField(this::class.java, "parameters")?.get(this)?.nullCast<Collection<Enum<*>>>()?.serializeParameter(),
            "style" to Util.MAPPER.convertValue<MutableMap<String, Any>>(getField(this::class.java, "style")?.get(this), JSON_MAP_TYPE).trimStyle(),
        )
    }

    fun Collection<Enum<*>>.serializeParameter(): List<Any> {
        val output: MutableList<Any> = mutableListOf()

        for (entry in this) {
            output += entry.name.lowercase()
        }

        return output
    }

    fun MutableMap<String, Any>.trimStyle(): Map<String, Any>? {
        if (this["empty"] == true) {
            return null
        }
        remove("empty")
        removeIf("underlined") { it == false }
        removeIf("strikethrough") { it == false }
        removeIf("bold") { it == false }
        removeIf("italic") { it == false }
        removeIf("obfuscated") { it == false }
        removeIf("font") { it == DEFAULT_FONT }

        this.remove("color")?.mapCast()?.let { color ->
            val name = color["name"]
            if (name != null) {
                this["color"] = name
                return@let
            }
            this["color"] = color["hexCode"]!!
        }

        return this
    }

    fun <K, V> MutableMap<K, V>.removeIf(key: K, check: (V?) -> Boolean) {
        if (check(this[key])) {
            remove(key)
        }
    }
}
