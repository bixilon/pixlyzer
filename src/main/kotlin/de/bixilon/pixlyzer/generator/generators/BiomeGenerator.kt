package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.ReflectionUtil.getGetter
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.mapCast
import de.bixilon.pixlyzer.util.Util.nullCast
import net.minecraft.sound.SoundEvent
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.world.biome.Biome
import java.util.*

object BiomeGenerator : Generator(
    "biomes",
    allowedFail = getClass("net.minecraft.world.biome.BuiltinBiomes") != null,
) {
    override fun generate() {
        for ((resourceLocation, id, biome) in getBiomes()) {
            val biomeData: MutableMap<Any, Any> = mutableMapOf(
                "id" to id,
                "water_color" to biome.waterColor, // do not clean up!
                "water_fog_color" to biome.waterFogColor, // do not clean up!
            )

            getField(Biome::class.java, "category")?.get(biome)?.let {
                check(it is Enum<*>)
                biomeData["category"] = Enum::class.java.getDeclaredMethod("ordinal").apply { isAccessible = true }.invoke(it)
            }

            BIOME_DEPTH_FIELD?.let { biomeData["depth"] = it.getFloat(biome) }
            BIOME_SCALE_FIELD?.let { biomeData["scale"] = it.getFloat(biome) }

            val climateParameters = CLIMATE_PARAMETERS_FIELD?.get(biome) ?: biome

            (PRECIPITATION_FIELD!!.get(climateParameters) as Enum<*>?)?.ordinal?.let {
                biomeData["precipitation"] = it
            }
            biomeData["temperature"] = TEMPERATURE_FIELD!!.getFloat(climateParameters)
            (TEMPERATURE_MODIFIER_FIELD?.get(climateParameters) as Enum<*>?)?.let {
                biomeData["temperature_modifier"] = it.name
            }
            biomeData["downfall"] = DOWNFALL_FIELD!!.getFloat(climateParameters)


            getField(Biome::class.java, "effects")?.get(biome)?.let {
                FOG_COLOR_SPECIAL_EFFECTS_FIELD?.getInt(it)?.let {
                    biomeData["fog_color"] = it
                }
                WATER_COLOR_SPECIAL_EFFECTS_FIELD?.getInt(it)?.let {
                    biomeData["water_color"] = it
                }
                WATER_FOG_COLOR_SPECIAL_EFFECTS_FIELD?.getInt(it)?.let {
                    biomeData["water_fog_color"] = it
                }
                SKY_COLOR_SPECIAL_EFFECTS_FIELD?.getInt(it)?.let {
                    biomeData["sky_color"] = it
                }
                (FOLIAGE_COLOR_OVERRIDE_SPECIAL_EFFECTS_FIELD?.get(it) as Optional<Int>?)?.ifPresent {
                    biomeData["foliage_color_override"] = it
                }
                (GRASS_COLOR_OVERRIDE_SPECIAL_EFFECTS_FIELD?.get(it) as Optional<Int>?)?.ifPresent {
                    biomeData["grass_color_override"] = it
                }
                (GRASS_COLOR_MODIFIER_SPECIAL_EFFECTS_FIELD?.get(it) as Enum<*>?)?.name?.let {
                    biomeData["grass_color_modifier"] = it
                }

                (AMBIENT_SOUND_LOOP_EVENT_SPECIAL_EFFECTS_FIELD?.get(it) as Optional<SoundEvent>?)?.ifPresent {
                    biomeData["ambient_sound_event_loop"] = SoundEventGenerator.REGISTRY.getRawId(it)
                }

                (BACKGROUND_MUSIC_SPECIAL_EFFECTS_FIELD?.get(it) as Optional<Any>?)?.ifPresent {
                    biomeData["background_music"] = SoundEventGenerator.REGISTRY.getRawId(MUSIC_CLASS_EVENT_FIELD!!.get(it) as SoundEvent)
                }
            }

            if (biomeData["foliage_color_override"] == null) {
                // calculate color overrider
                try {
                    biomeData["foliage_color_override"] = FOLIAGE_COLOR_AT_METHOD_METHOD!!.invoke(biome, null) as Int
                } catch (exception: Exception) {
                    try {
                        biomeData["foliage_color_override"] = FOLIAGE_COLOR_METHOD!!.invoke(biome) as Int
                    } catch (exception: Exception) {
                    }
                }
            }
            biomeData["foliage_color_override"]?.nullCast<Int>()?.let {
                if (it == 0) {
                    biomeData.remove("foliage_color_override")
                }
            }
            if (biomeData["grass_color_override"] == null) {
                try {
                    biomeData["grass_color_override"] = GRASS_COLOR_OVERRIDE_METHOD!!.invoke(biome, null) as Int
                } catch (exception: Exception) {
                }
            }
            if (biomeData["sky_color"] == null) {
                BIOME_SKY_COLOR_FIELD?.getInt(biome)?.let {
                    biomeData["sky_color"] = it
                } ?: let {
                    SKY_COLOR_GETTER?.invoke(biome)
                } ?: let {
                    biomeData["sky_color"] = SKY_COLOR_FUNCTION!!.invoke(biome, 0.0f) as Int
                }
            }

            data[resourceLocation.toString()] = biomeData
        }
    }

    private val FOLIAGE_COLOR_AT_METHOD_METHOD = try {
        Biome::class.java.getMethod("getFoliageColorAt", BlockPos::class.java)
    } catch (exception: Exception) {
        null
    }

    private val FOLIAGE_COLOR_METHOD = try {
        Biome::class.java.getMethod("getFoliageColor")
    } catch (exception: Exception) {
        try {
            Biome::class.java.getMethod("getFoliageColorAt")
        } catch (exception: Exception) {
            null
        }
    }
    private val GRASS_COLOR_OVERRIDE_METHOD = try {
        Biome::class.java.getMethod("getGrassColorAt", BlockPos::class.java)
    } catch (exception: Exception) {
        null
    }
    private val SKY_COLOR_GETTER = getGetter(Biome::class.java, "getSkyColor")

    private val SKY_COLOR_FUNCTION = try {
        Biome::class.java.getDeclaredMethod("getSkyColor", Float::class.java)
    } catch (exception: Exception) {
        null
    }

    private val SPECIAL_EFFECTS_CLASS = getClass("net.minecraft.world.biome.BiomeEffects")
    private val CLIMATE_SETTINGS_CLASS = getClass("net.minecraft.world.biome.Biome\$Weather", "net.minecraft.world.level.biome.Biome\$ClimateSettings", "net.minecraft.world.level.biome.Biome", "net.minecraft.world.biome.Biome")!!

    private val FOG_COLOR_SPECIAL_EFFECTS_FIELD = getField(SPECIAL_EFFECTS_CLASS, "fogColor")
    private val WATER_COLOR_SPECIAL_EFFECTS_FIELD = getField(SPECIAL_EFFECTS_CLASS, "waterColor")
    private val WATER_FOG_COLOR_SPECIAL_EFFECTS_FIELD = getField(SPECIAL_EFFECTS_CLASS, "waterFogColor")
    private val SKY_COLOR_SPECIAL_EFFECTS_FIELD = getField(SPECIAL_EFFECTS_CLASS, "skyColor")
    private val FOLIAGE_COLOR_OVERRIDE_SPECIAL_EFFECTS_FIELD = getField(SPECIAL_EFFECTS_CLASS, "foliageColor")
    private val GRASS_COLOR_OVERRIDE_SPECIAL_EFFECTS_FIELD = getField(SPECIAL_EFFECTS_CLASS, "grassColor")
    private val GRASS_COLOR_MODIFIER_SPECIAL_EFFECTS_FIELD = getField(SPECIAL_EFFECTS_CLASS, "grassColorModifier")
    private val AMBIENT_SOUND_LOOP_EVENT_SPECIAL_EFFECTS_FIELD = getField(SPECIAL_EFFECTS_CLASS, "loopSound")
    private val BACKGROUND_MUSIC_SPECIAL_EFFECTS_FIELD = getField(SPECIAL_EFFECTS_CLASS, "music")


    private val CLIMATE_PARAMETERS_FIELD = getField(Biome::class.java, "weather", "climateSettings")
    private val PRECIPITATION_FIELD = getField(CLIMATE_SETTINGS_CLASS, "precipitation")
    private val TEMPERATURE_FIELD = getField(CLIMATE_SETTINGS_CLASS, "temperature", "baseTemperature")
    private val TEMPERATURE_MODIFIER_FIELD = getField(CLIMATE_SETTINGS_CLASS, "temperatureModifier")
    private val DOWNFALL_FIELD = getField(CLIMATE_SETTINGS_CLASS, "downfall")


    private val BIOME_SKY_COLOR_FIELD = getField(Biome::class.java, "field_21806", "skyColor")


    private val BIOME_DEPTH_FIELD = getField(Biome::class.java, "depth")
    private val BIOME_SCALE_FIELD = getField(Biome::class.java, "scale")


    private val MUSIC_CLASS_EVENT_FIELD = getField(getClass("net.minecraft.sound.MusicSound"), "sound", "event")

    private fun getBiomes(): Set<Triple<Identifier, Int, Biome>> {
        val biomes = RegistryUtil.getRegistry<Biome>("BIOME")!!

        val ret: MutableSet<Triple<Identifier, Int, Biome>> = mutableSetOf()

        for (biome in biomes) {
            ret.add(Triple(biomes.getId(biome)!!, biomes.getRawId(biome), biome))
        }

        return ret
    }


    override fun test() {
        data["minecraft:ocean"]!!
        data["minecraft:taiga"]!!

        check(data["minecraft:swamp"]!!.mapCast()!!["foliage_color_override"] == 6975545)
    }
}
