package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.PixLyzer
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.generator.generators.FeatureGenerator.serializeFeatureSet
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.ReflectionUtil.getGetter
import de.bixilon.pixlyzer.util.ReflectionUtil.variable
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import de.bixilon.pixlyzer.util.Util.listCast
import de.bixilon.pixlyzer.util.Util.nullCast
import de.bixilon.pixlyzer.util.Util.realName
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.entity.EntityType
import net.minecraft.entity.EquipmentSlot
import net.minecraft.fluid.Fluid
import net.minecraft.item.*
import net.minecraft.sound.SoundEvent
import java.lang.reflect.InvocationTargetException
import java.util.*

object ItemGenerator : Generator(
    "items"
) {
    val REGISTRY = RegistryUtil.getRegistry<Item>("ITEM")!!

    override fun generate() {
        for (item in REGISTRY) {
            val resourceLocation = REGISTRY.getId(item)
            val itemData = compound()
            itemData["id"] = REGISTRY.getRawId(item)
            item.getRarity(ItemStack.EMPTY).ordinal.let {
                if (it != 0) {
                    itemData["rarity"] = it
                }
            }
            if (item.maxCount != 64) {
                itemData["max_stack_size"] = item.maxCount
            }
            if (item.maxDamage > 0) {
                itemData["max_damage"] = item.maxDamage
            }
            IS_FIRE_RESISTANT_FIELD?.let {
                if (it.getBoolean(item)) {
                    itemData["is_fire_resistant"] = true
                }
            }
            if (!item.isNetworkSynced) {
                itemData["is_complex"] = item.isNetworkSynced
            }
            item.getTranslationKey()?.let {
                itemData["translation_key"] = it
            }
            item.foodComponent?.let {
                itemData["food_properties"] = it.serialize()
            }
            // itemData["default_destroy_speed"] =  item.getDestroySpeed(ItemStack.EMPTY, Registry.BLOCK.get(Registry.BLOCK.defaultKey.defaultBlockState()))

            REQUIRED_FEATURES?.get(item)?.serializeFeatureSet()?.let { itemData["features"] = it }


            if (item is BlockItem) {
                itemData["block"] = BlockGenerator.REGISTRY.getRawId(item.block)
            }
            if (item is ToolItem) {
                itemData["uses"] = item.material.durability
                itemData["speed"] = TOOL_ITEM_MINING_SPEED_GETTER.invoke(item.material) as Float
                itemData["attack_damage_bonus"] = item.material.attackDamage
                itemData["level"] = item.material.miningLevel
                itemData["enchantment_value"] = item.material.enchantability
                // ToDo itemData["repair_ingredient"] =  item.tier.repairIngredient.serialize()
            }
            if (item is MiningToolItem) {
                val blocks = mutableListOf<Int>()
                (DIGGER_ITEM_BLOCKS_FIELD.get(item)?.let {
                    val ret = if (it::class.java == PixLyzer.TAG_CLASS) {
                        null // ToDo
                        // (it as Tag<Block>).values()
                    } else if (it is Set<*>) {
                        (it as Set<Block>)
                    } else {
                        null
                    }
                    ret?.toSortedSet { block: Block, block1: Block -> BlockGenerator.REGISTRY.getRawId(block) - BlockGenerator.REGISTRY.getRawId(block1) }
                })?.let {
                    for (block in it) {
                        blocks.add(BlockGenerator.REGISTRY.getRawId(block))
                    }
                }
                if (blocks.size > 0) {
                    itemData["diggable_blocks"] = blocks
                }
                itemData["speed"] = DIGGER_ITEM_SPEED_FIELD.getFloat(item)
                itemData["attack_damage"] = DIGGER_ITEM_ATTACK_DAMAGE.getFloat(item)
            }
            if (item is ArmorItem) {
                (ARMOR_ITEM_EQUIPMENT_SLOT_FIELD?.get(item) as EquipmentSlot?)?.let {
                    itemData["equipment_slot"] = it.name.lowercase(Locale.getDefault())
                }
                itemData["defense"] = item.protection
                itemData["toughness"] = ARMOR_ITEM_TOUGHNESS_FIELD.getFloat(item)
                itemData["armor_material"] = item.material.name.lowercase(Locale.getDefault())
                ARMOR_ITEM_KNOCKBACK_RESISTANCE?.getFloat(item)?.let {
                    itemData["knockback_resistance"] = it
                }
            }

            if (item is AxeItem) {
                val strippables = compound()
                (AXE_ITEM_STRIPPABLES_FIELD.get(item) as Map<Block, Block>?)?.toSortedMap { block: Block, block1: Block -> BlockGenerator.REGISTRY.getRawId(block) - BlockGenerator.REGISTRY.getRawId(block1) }?.let {
                    for ((origin, target) in it) {
                        strippables[BlockGenerator.REGISTRY.getRawId(origin)] = BlockGenerator.REGISTRY.getRawId(target)
                    }
                }
                if (strippables.isNotEmpty()) {
                    itemData["strippables_blocks"] = strippables
                }

                AXE_EFFECTIVE_MATERIALS?.get(item)?.nullCast<Collection<Any>>()?.let {
                    val effectiveMaterials = mutableListOf<String>()

                    for (material in it) {
                        effectiveMaterials.add(MaterialGenerator.MATERIALS.inverse()[material].toString())
                    }
                    if (effectiveMaterials.size > 0) {
                        itemData["effective_materials"] = effectiveMaterials.toSortedSet()
                    }
                }
            }
            if (item is BucketItem) {
                (BUCKED_ITEM_CONTENT_FIELD.get(item) as Fluid?)?.let {
                    itemData["bucked_fluid_type"] = FluidGenerator.REGISTRY.getRawId(it)
                }
            }
            if (item is DyeItem) {
                itemData["dye_color"] = item.color.name.lowercase(Locale.getDefault())
            }
            if (item::class.java == MOB_BUCKED_ITEM_CLASS) {
                (MOB_BUCKED_ITEM_TYPE_FIELD?.get(item) as EntityType<*>?)?.let {
                    itemData["bucket_fish_type"] = EntityGenerator.REGISTRY.getRawId(it)
                }
                (MOB_BUCKED_ITEM_EMPTY_SOUND_FIELD?.get(item) as SoundEvent?)?.let {
                    itemData["bucket_empty_sound"] = SoundEventGenerator.REGISTRY.getRawId(it)
                }
            }
            if (item is HoeItem) {
                val tillables = compound()
                try {
                    // ToDo
                    HOE_ITEM_TILLABLES_FIELD?.get(item)?.nullCast<Map<Block, BlockState>>()?.toSortedMap { block: Block, block1: Block -> BlockGenerator.REGISTRY.getRawId(block) - BlockGenerator.REGISTRY.getRawId(block1) }?.let {
                        for ((origin, target) in it) {
                            tillables[BlockGenerator.REGISTRY.getRawId(origin)] = Block.getRawIdFromState(target)
                            tillables[BlockGenerator.REGISTRY.getRawId(origin)] = Block.getRawIdFromState(target)
                        }
                    }
                } catch (exception: ClassCastException) {
                }
                if (tillables.isNotEmpty()) {
                    itemData["tillables_block_states"] = tillables
                }
            }
            if (item is HorseArmorItem) {
                itemData["horse_protection"] = item.bonus
                itemData["horse_texture"] = item.entityTexture.toString()
            }
            if (item is ShovelItem) {
                val flattenables = compound()
                (SHOVEL_ITEM_FLATTENABLES_FIELD.get(item) as Map<Block, BlockState>?)?.toSortedMap { block: Block, block1: Block -> BlockGenerator.REGISTRY.getRawId(block) - BlockGenerator.REGISTRY.getRawId(block1) }?.let {
                    for ((origin, target) in it) {
                        flattenables[BlockGenerator.REGISTRY.getRawId(origin)] = Block.getRawIdFromState(target)
                    }
                }
                if (flattenables.isNotEmpty()) {
                    itemData["flattenables_block_states"] = flattenables
                }
            }
            let {
                if (item is PickaxeItem) {
                    val diggableStates: MutableSet<Int> = mutableSetOf()
                    val diggableBlocks: MutableSet<Int> = mutableSetOf()
                    for (block in BlockGenerator.REGISTRY) {
                        val states: MutableSet<Int> = mutableSetOf()
                        for (state in block.variable<Any>("stateManager", "stateFactory")!!.variable<List<BlockState>>("states")!!) {
                            try {
                                if (PICKAXE_EFFECTIVE_ON_METHOD.invoke(item, state) as Boolean) {
                                    states += Block.getRawIdFromState(state)
                                }
                            } catch (exception: InvocationTargetException) {
                                return@let
                            }
                        }
                        if (states.size == block.variable<Any>("stateManager", "stateFactory")!!.variable<List<BlockState>>("states")!!.size) {
                            // all states are diggable
                            diggableBlocks += BlockGenerator.REGISTRY.getRawId(block)
                        } else {
                            diggableStates.addAll(states)
                        }
                    }
                    if (diggableStates.isNotEmpty()) {
                        itemData["diggable_states"] = diggableStates.toSortedSet()
                    }
                    if (diggableBlocks.isNotEmpty()) {
                        itemData["diggable_blocks"]?.listCast<Int>()?.let {
                            // merge and remove duplicates
                            for (id in it) {
                                diggableBlocks.add(id)
                            }
                        }
                        itemData["diggable_blocks"] = diggableBlocks.toSortedSet()
                    }
                }
            }
            if (item is SpawnEggItem) {
                itemData["spawn_egg_color_1"] = item.getColor(0)
                itemData["spawn_egg_color_2"] = item.getColor(1)
                (SPAWN_EGG_ITEM_ENTITY_TYPE_FIELD.get(item) as EntityType<*>?)?.let {
                    itemData["spawn_egg_entity_type"] = EntityGenerator.REGISTRY.getRawId(it)
                }
            }
            if (item is SwordItem) {
                itemData["attack_damage"] = item.attackDamage
            }

            if (item is MusicDiscItem) {
                itemData["analog_output"] = item.comparatorOutput
                itemData["sound"] = SoundEventGenerator.REGISTRY.getRawId(item.sound)
            }


            val className = item::class.java.realName
            if (className != "Item") {
                itemData["class"] = className
            }

            data[resourceLocation.toString()] = itemData
        }
    }

    private fun FoodComponent.serialize(): MutableMap<Any, Any> {
        val data = compound()
        data["nutrition"] = hunger
        data["saturation_modifier"] = saturationModifier
        data["is_meat"] = isMeat
        data["can_always_eat"] = isAlwaysEdible
        data["fast_food"] = isSnack
        return data
    }

    private val PICKAXE_EFFECTIVE_ON_METHOD = try {
        PickaxeItem::class.java.getMethod("isEffectiveOn", BlockState::class.java)
    } catch (exception: NoSuchMethodException) {
        PickaxeItem::class.java.getMethod("isSuitableFor", BlockState::class.java)
    }

    private val AXE_EFFECTIVE_MATERIALS = getField(AxeItem::class.java, "field_23139")

    private val MOB_BUCKED_ITEM_CLASS = getClass("net.minecraft.world.item.MobBucketItem", "net.minecraft.world.item.BucketItem")

    private val TOOL_ITEM_MINING_SPEED_GETTER = getGetter(ToolMaterial::class.java, "getMiningSpeed", "getMiningSpeedMultiplier")!!


    private val IS_FIRE_RESISTANT_FIELD = getField(Item::class.java, "fireproof")

    private val DIGGER_ITEM_BLOCKS_FIELD = MiningToolItem::class.java.getDeclaredField("effectiveBlocks")
    private val DIGGER_ITEM_SPEED_FIELD = MiningToolItem::class.java.getDeclaredField("miningSpeed")
    private val DIGGER_ITEM_ATTACK_DAMAGE =
        getField(MiningToolItem::class.java, "attackDamageBaseline", "attackDamage")!!
    private val ARMOR_ITEM_EQUIPMENT_SLOT_FIELD = getField(ArmorItem::class.java, "slot")
    private val ARMOR_ITEM_TOUGHNESS_FIELD = ArmorItem::class.java.getDeclaredField("toughness")
    private val AXE_ITEM_STRIPPABLES_FIELD = getField(AxeItem::class.java, "STRIPABLES", "STRIPPABLES", "STRIPPED_BLOCKS")!!
    private val BUCKED_ITEM_CONTENT_FIELD = BucketItem::class.java.getDeclaredField("fluid")
    private val MOB_BUCKED_ITEM_TYPE_FIELD = getField(MOB_BUCKED_ITEM_CLASS, "type")
    private val MOB_BUCKED_ITEM_EMPTY_SOUND_FIELD = getField(MOB_BUCKED_ITEM_CLASS, "emptySound")
    private val HOE_ITEM_TILLABLES_FIELD = getField(HoeItem::class.java, "TILLED_BLOCKS")
    private val SHOVEL_ITEM_FLATTENABLES_FIELD = getField(ShovelItem::class.java, "PATH_STATES", "PATH_BLOCKSTATES")!!
    private val SPAWN_EGG_ITEM_ENTITY_TYPE_FIELD = SpawnEggItem::class.java.getDeclaredField("type")
    private val ARMOR_ITEM_KNOCKBACK_RESISTANCE = getField(ArmorItem::class.java, "knockbackResistance")

    private val REQUIRED_FEATURES = getField(Item::class.java, "requiredFeatures")

    init {
        DIGGER_ITEM_BLOCKS_FIELD.isAccessible = true
        DIGGER_ITEM_SPEED_FIELD.isAccessible = true
        ARMOR_ITEM_TOUGHNESS_FIELD.isAccessible = true
        AXE_ITEM_STRIPPABLES_FIELD.isAccessible = true
        BUCKED_ITEM_CONTENT_FIELD.isAccessible = true
        SHOVEL_ITEM_FLATTENABLES_FIELD.isAccessible = true
        SPAWN_EGG_ITEM_ENTITY_TYPE_FIELD.isAccessible = true
    }
}
