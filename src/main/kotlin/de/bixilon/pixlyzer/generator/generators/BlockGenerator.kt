package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.EntitySpawner
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.generator.generators.FeatureGenerator.serializeFeatureSet
import de.bixilon.pixlyzer.generator.generators.VoxelShapeGenerator.serialize
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.ReflectionUtil.getGetter
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import de.bixilon.pixlyzer.util.Util.realName
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.Blocks
import net.minecraft.block.FluidBlock
import net.minecraft.client.color.block.BlockColors
import net.minecraft.fluid.Fluid
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3d
import net.minecraft.util.shape.VoxelShape
import net.minecraft.world.BlockView
import net.minecraft.world.World
import java.util.*

object BlockGenerator : Generator(
    "blocks"
) {
    val REGISTRY = RegistryUtil.getRegistry<Block>("BLOCK")!!
    val VOXEL_SHAPES: MutableMap<Any, Int> = mutableMapOf()

    override fun generate() {
        val stateMap: MutableMap<Block, MutableSet<BlockState>> = mutableMapOf()

        for (state in BLOCK_STATE_REGISTRY) {
            val owner = BLOCK_STATE_OWNER_FIELD.get(state) as Block
            val blockSet = stateMap[owner] ?: mutableSetOf()
            if (blockSet.isEmpty()) {
                stateMap[owner] = blockSet
            }
            blockSet.add(state)
        }

        val waterCauldronBlock = REGISTRY.get(Identifier("minecraft:water_cauldron")) as Block?

        for (block in REGISTRY) {
            val resourceLocation = REGISTRY.getId(block)
            val blockData = compound()
            blockData["id"] = REGISTRY.getRawId(block)
            blockData["explosion_resistance"] = block.blastResistance
            blockData["item"] = ItemGenerator.REGISTRY.getRawId(block.asItem())
            if (block.slipperiness != 0.6f) {
                blockData["friction"] = block.slipperiness
            }
            VELOCITY_MULTIPLIER_FIELD?.getFloat(block)?.let {
                if (it != 1.0f) {
                    blockData["velocity_multiplier"] = it
                }
            }
            JUMP_VELOCITY_MULTIPLIER_FIELD?.getFloat(block)?.let {
                if (it != 1.0f) {
                    blockData["jump_velocity_multiplier"] = it
                }
            }
            BLOCK_STATE_REQUIRES_TOOL?.getBoolean(block.defaultState)?.let { if (it) blockData["requires_tool"] = it }
            BLOCK_STATE_REPLACEABLE?.getBoolean(block.defaultState)?.let { if (it) blockData["replaceable"] = it }
            blockData["default_state"] = Block.getRawIdFromState(block.defaultState)
            if (block.hasDynamicBounds()) {
                blockData["has_dynamic_shape"] = block.hasDynamicBounds()
            }

            val className = block::class.java.realName
            if (className != "Block") {
                blockData["class"] = className
            }
            blockData["sound_group"] = SoundGroupGenerator.getId(block.soundGroup)

            if (block is FluidBlock) {
                (FLUID_BLOCK_FLUID_FIELD.get(block) as Fluid).let {
                    if (FLOWABLE_FLUID_CLASS.isAssignableFrom(it::class.java)) {
                        blockData["still_fluid"] = FluidGenerator.REGISTRY.getRawId(FLOWABLE_GET_STILL_METHOD.invoke(it) as Fluid)
                        blockData["flow_fluid"] = FluidGenerator.REGISTRY.getRawId(FLOWABLE_GET_FLOWING_METHOD.invoke(it) as Fluid)
                    } else {
                        blockData["fluid"] = FluidGenerator.REGISTRY.getRawId(it)
                    }
                }
            }


            block.putOffsetType(blockData)

            val hasColorProperties = (TINT_PROPERTIES_METHOD?.invoke(DEFAULT_BLOCK_COLORS, block) as Set<*>?)?.size?.let { it > 0 } ?: let {
                val blockColorProviderList = BLOCK_COLORS_PROVIDERS_ID_LIST!!.get(DEFAULT_BLOCK_COLORS)
                ID_LIST_GET_METHOD.invoke(blockColorProviderList, REGISTRY.getRawId(block)) != null
            }

            MATERIAL?.let { blockData["material"] = MaterialGenerator.MATERIALS.inverse()[it.invoke(block)].toString() }
            blockData["hardness"] = block.hardness

            val states = compound()

            // tints
            when (block) {
                Blocks.LARGE_FERN, Blocks.TALL_GRASS -> {
                    blockData["tint"] = "minecraft:shearing_double_plant_tint"
                }

                Blocks.GRASS_BLOCK, Blocks.FERN, Blocks.GRASS, Blocks.POTTED_FERN -> {
                    blockData["tint"] = "minecraft:grass_tint"
                }

                Blocks.SPRUCE_LEAVES, Blocks.BIRCH_LEAVES, Blocks.ATTACHED_MELON_STEM, Blocks.ATTACHED_PUMPKIN_STEM -> {
                    blockData["tint_color"] = DEFAULT_BLOCK_COLORS.getColor(block.defaultState, null, null, 1)
                }

                Blocks.OAK_LEAVES, Blocks.JUNGLE_LEAVES, Blocks.ACACIA_LEAVES, Blocks.DARK_OAK_LEAVES, Blocks.VINE -> {
                    blockData["tint"] = "minecraft:foliage_tint"
                }

                Blocks.WATER, Blocks.BUBBLE_COLUMN, Blocks.CAULDRON, waterCauldronBlock -> {
                    blockData["tint"] = "minecraft:water_tint"
                }

                Blocks.SUGAR_CANE -> {
                    blockData["tint"] = "minecraft:sugar_cane_tint"
                }

                Blocks.LILY_PAD -> {
                    blockData["tint"] = "minecraft:lily_pad_tint"
                }
            }

            REQUIRED_FEATURES?.get(block)?.serializeFeatureSet()?.let { blockData["features"] = it }


            for (state in stateMap[block]!!) {
                val stateData = compound()

                state.putOffsetType(stateData)

                if (state.luminance != 0) {
                    stateData["luminance"] = state.luminance
                }
                if (state.hasRandomTicks()) {
                    stateData["is_randomly_ticking"] = true
                }
                if (state.hasSidedTransparency()) {
                    stateData["has_side_transparency"] = true
                }

                REQUIRES_CORRECT_TOOL_FOR_DROP_FIELDS?.let {
                    if (it.getBoolean(state)) {
                        stateData["requires_tool"] = true
                    }
                }


                if (!state.isOpaque) {
                    stateData["is_opaque"] = state.isOpaque
                }


                try {
                    if (hasColorProperties) {
                        val tintColor = (BLOCK_COLOR_GET_TINT_METHOD_NEW?.invoke(DEFAULT_BLOCK_COLORS, state, null, null, 0) ?: BLOCK_COLOR_GET_TINT_METHOD_OLD?.invoke(DEFAULT_BLOCK_COLORS, state, null, null)) as Int
                        when (blockData["tint"]?.toString()) {
                            "minecraft:foliage_tint", "minecraft:lily_pad_tint", "minecraft:grass_tint" -> {
                            }

                            else -> {
                                if (tintColor != -1 && tintColor != 0) {
                                    stateData["tint_color"] = tintColor
                                }
                            }
                        }
                    }
                } catch (exception: Exception) {
                }

                val propertyData = compound()

                for ((property, stateProperty) in state.entries) {
                    val propertyValue = (PROPERTY_NAME_METHOD.invoke(property) as String).lowercase(Locale.getDefault())
                    val propertyName = stateProperty.toString().lowercase(Locale.getDefault())
                    addJsonWithType(propertyName, propertyData, propertyValue)
                }

                if (propertyData.isNotEmpty()) {
                    stateData["properties"] = propertyData
                }

                state.initShapeCache()

                val positionOffset = BLOCK_STATE_GET_OFFSET.invoke(state, EntitySpawner.CLIENT_LEVEL, EMPTY_BLOCK_POSITION) as Vec3d


                fun VoxelShape.add(name: String): VoxelShape {
                    if (isEmpty) {
                        return this
                    }
                    stateData[name] = this.offset(-positionOffset.x, -positionOffset.y, -positionOffset.z).createId()
                    return this
                }

                state.getCollisionShape(EntitySpawner.CLIENT_LEVEL, EMPTY_BLOCK_POSITION).add("collision_shape")
                if (className != "PitcherCropBlock") { // 23w14a: crash because age does not match?
                    state.getOutlineShape(EntitySpawner.CLIENT_LEVEL, EMPTY_BLOCK_POSITION).add("outline_shape")
                }
                // ToDo state.getSidesShape(EntitySpawner.CLIENT_LEVEL, EMPTY_BLOCK_POSITION).add("sides_shape")
                (BLOCK_STATE_GET_RAYCAST_SHAPE?.invoke(EntitySpawner.CLIENT_LEVEL, EMPTY_BLOCK_POSITION) as VoxelShape?)?.add("raycast_shape")


                if (!block.hasDynamicBounds()) {
                    // cache
                    // ToDo: Still need this data on dynamic shapes :(

                    val cache = BLOCK_STATE_CACHE_FIELD.get(state)

                    CACHE_SOLID_RENDER_FIELD.getBoolean(cache).let {
                        if (!it) {
                            return@let
                        }
                        stateData["solid_render"] = it
                    }

                    CACHE_TRANSLUCENT_FIELD!!.getBoolean(cache).let {
                        if (it) {
                            return@let
                        }

                        stateData["translucent"] = CACHE_TRANSLUCENT_FIELD.getBoolean(cache)
                    }

                    CACHE_LIGHT_BLOCK_FIELD.getInt(cache).let {
                        if (it == 0) {
                            return@let
                        }
                        stateData["light_block"] = it
                    }

                    LARGE_COLLISION_SHAPE_FIELD.getBoolean(cache).let {
                        if (!it) {
                            return@let
                        }
                        stateData["large_collision_shape"] = it
                    }
                    IS_COLLISION_SHAPE_FULL_BLOCK?.getBoolean(cache)?.let {
                        if (!it) {
                            return@let
                        }
                        stateData["is_collision_shape_full_block"] = it
                    }

                    (IS_FACE_STURDY?.get(cache) as BooleanArray?)?.let {
                        if (allTheSame(it)) {
                            stateData["is_sturdy"] = it[0]
                            return@let
                        }

                        val sturdy = mutableListOf<Boolean>()

                        for (bool in it) {
                            sturdy.add(bool)
                        }
                        stateData["is_sturdy"] = sturdy
                    }
                }

                states[Block.getRawIdFromState(state)] = stateData
            }

            blockData["states"] = states



            data[resourceLocation.toString()] = blockData
        }
    }

    private val EMPTY_BLOCK_POSITION = BlockPos(0, 0, 0)

    private fun Any.putOffsetType(data: MutableMap<Any, Any>) {
        if ((this is Block && BLOCK_GET_OFFSET_TYPE == null) || (this is BlockState && BLOCK_STATE_OFFSET_FIELD == null)) {
            return
        }
        val type = (BLOCK_GET_OFFSET_TYPE ?: BLOCK_STATE_OFFSET_FIELD)?.invoke(this) ?: return
        val offsetType = OFFSET_TYPE_NAME_METHOD.invoke(type) as String
        if (offsetType != "NONE") {
            data["offset_type"] = offsetType.lowercase(Locale.getDefault())
            BLOCK_GET_MAX_OFFSET?.invoke(this)?.let {
                data["max_model_offset"] = it as Float
            }
        }
    }

    private val BLOCK_STATE_GET_RAYCAST_SHAPE = try {
        BlockState::class.java.getMethod("getRaycastShape", World::class.java, BlockPos::class.java)
    } catch (exception: Exception) {
        null
    }

    private val BLOCK_STATE_GET_OFFSET = try {
        BlockState::class.java.getMethod("getModelOffset", BlockView::class.java, BlockPos::class.java)
    } catch (exception: Exception) {
        BlockState::class.java.getMethod("getOffsetPos", BlockView::class.java, BlockPos::class.java)
    }

    private val MATERIAL = getGetter(Block::class.java, "getMaterial")


    private val BLOCK_GET_MAX_OFFSET = getGetter(getClass("net.minecraft.block.AbstractBlock"), "getMaxModelOffset")
    private val BLOCK_GET_OFFSET_TYPE = getClass("net.minecraft.block.AbstractBlock", "net.minecraft.block.Block")!!.getGetter("getOffsetType")
    private val OFFSET_TYPE_NAME_METHOD = getClass("net.minecraft.block.AbstractBlock\$OffsetType", "net.minecraft.block.Block\$OffsetType")!!.getMethod("name")


    private val ID_LIST_CLASS = getClass("net.minecraft.util.IdList", "net.minecraft.util.collection.IdList")!!

    private val ID_LIST_GET_METHOD = ID_LIST_CLASS.getDeclaredMethod("get", Int::class.java)

    private val BLOCK_COLORS_PROVIDERS_ID_LIST = getField(BlockColors::class.java, "providers")

    private val TINT_PROPERTIES_METHOD = try {
        BlockColors::class.java.getDeclaredMethod("method_21592")
    } catch (exception: Exception) {
        null
    }


    private val PROPERTY_METHOD = getClass("net.minecraft.state.property.Property")!!

    private val PROPERTY_NAME_METHOD = PROPERTY_METHOD.getDeclaredMethod("getName")

    private fun VoxelShape.createId(): Int {
        return VOXEL_SHAPES.getOrPut(this.serialize()) { VOXEL_SHAPES.size }
    }

    private fun allTheSame(array: BooleanArray): Boolean {
        val firstValue = array[0]
        for (value in array) {
            if (value != firstValue) {
                return false
            }
        }
        return true
    }

    private val BLOCK_STATE_BASE_CLASS = getClass("net.minecraft.block.AbstractBlock\$AbstractBlockState", "net.minecraft.world.level.block.state.BlockBehaviour\$BlockStateBase", "net.minecraft.world.level.block.state.BlockState", "net.minecraft.block.BlockState")!!
    private val BLOCK_STATE_OWNER_FIELD = getField(getClass("net.minecraft.state.AbstractPropertyContainer"), "owner") ?: getField(getClass("net.minecraft.state.State"), "owner") ?: getField(getClass("net.minecraft.state.AbstractState"), "owner") ?: getField(getClass("net.minecraft.world.level.block.state.AbstractStateHolder"), "owner")!!
    private val BLOCK_STATE_CACHE_FIELD = BLOCK_STATE_BASE_CLASS.getDeclaredField("shapeCache")

    private val REQUIRES_CORRECT_TOOL_FOR_DROP_FIELDS = getField(getClass("net.minecraft.block.AbstractBlock\$AbstractBlockState", "net.minecraft.block.BlockState"), "toolRequired")
    private val BLOCK_STATE_OFFSET_FIELD = BLOCK_STATE_BASE_CLASS.getGetter("getOffsetType", "method_43280")

    private val BLOCK_STATE_REQUIRES_TOOL = getField(BLOCK_STATE_BASE_CLASS, "toolRequired")
    private val BLOCK_STATE_REPLACEABLE = getField(BLOCK_STATE_BASE_CLASS, "replaceable")

    private lateinit var BLOCK_STATE_CACHE_CLASS: Class<*>

    init {
        for (clazz in BLOCK_STATE_BASE_CLASS.declaredClasses) {
            if (clazz.name == "net.minecraft.block.AbstractBlock\$AbstractBlockState\$ShapeCache" || clazz.name == "net.minecraft.world.level.block.state.BlockBehaviour\$BlockStateBase\$Cache" || clazz.name == "net.minecraft.world.level.block.state.BlockState\$Cache" || clazz.name == "net.minecraft.block.BlockState\$ShapeCache") {
                BLOCK_STATE_CACHE_CLASS = clazz
                break
            }
        }
    }

    private val FLOWABLE_FLUID_CLASS = getClass("net.minecraft.fluid.BaseFluid", "net.minecraft.fluid.FlowableFluid")!!

    private val FLOWABLE_GET_STILL_METHOD = getGetter(FLOWABLE_FLUID_CLASS, "getStill")!!
    private val FLOWABLE_GET_FLOWING_METHOD = getGetter(FLOWABLE_FLUID_CLASS, "getFlowing")!!

    private val FLUID_BLOCK_FLUID_FIELD = getField(FluidBlock::class.java, "fluid")!!
    private val CACHE_SOLID_RENDER_FIELD = BLOCK_STATE_CACHE_CLASS.getDeclaredField("fullOpaque")
    private val CACHE_TRANSLUCENT_FIELD = getField(BLOCK_STATE_CACHE_CLASS, "transparent", "translucent")
    private val CACHE_LIGHT_BLOCK_FIELD = BLOCK_STATE_CACHE_CLASS.getDeclaredField("lightSubtracted")
    private val LARGE_COLLISION_SHAPE_FIELD = getField(BLOCK_STATE_CACHE_CLASS, "exceedsCube", "field_17651")!!
    private val IS_FACE_STURDY = getField(BLOCK_STATE_CACHE_CLASS, "faceSturdy", "isFaceSturdy", "solidFullSquare")
    private val IS_COLLISION_SHAPE_FULL_BLOCK = getField(BLOCK_STATE_CACHE_CLASS, "exceedsCube", "shapeIsFullCube", "field_17651")


    private val VELOCITY_MULTIPLIER_FIELD = getField(Block::class.java, "velocityMultiplier")
    private val JUMP_VELOCITY_MULTIPLIER_FIELD = getField(Block::class.java, "jumpVelocityMultiplier")

    private val REQUIRED_FEATURES = getField(getClass("net.minecraft.block.AbstractBlock"), "requiredFeatures")


    init {
        BLOCK_STATE_CACHE_FIELD.isAccessible = true

        CACHE_SOLID_RENDER_FIELD.isAccessible = true
        CACHE_LIGHT_BLOCK_FIELD.isAccessible = true
        LARGE_COLLISION_SHAPE_FIELD.isAccessible = true
    }

    private val BLOCK_STATE_REGISTRY: Iterable<BlockState> = Block::class.java.getDeclaredField("STATE_IDS").get(null) as Iterable<BlockState>


    private val DEFAULT_BLOCK_COLORS = BlockColors.create()

    private val BLOCK_COLOR_GET_TINT_METHOD_NEW = try {
        BlockColors::class.java.getMethod("getColor", BlockState::class.java, Object::class.java, BlockPos::class.java, Int::class.java)
    } catch (exception: Exception) {
        null
    }
    private val BLOCK_COLOR_GET_TINT_METHOD_OLD = try {
        BlockColors::class.java.getMethod("getColor", BlockState::class.java, Object::class.java, BlockPos::class.java)
    } catch (exception: Exception) {
        null
    }


    fun addJsonWithType(input: String, output: MutableMap<Any, Any>, name: String) {
        try {
            output[name] = Integer.parseInt(input)
            return
        } catch (exception: Exception) {
        }

        try {
            val boolean = when (input) {
                "true" -> true
                "false" -> false
                else -> throw Exception("Not a boolean")
            }
            output[name] = boolean
            return
        } catch (exception: Exception) {
        }

        output[name] = input
    }
}

