package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import de.bixilon.pixlyzer.util.Util.realName
import net.minecraft.fluid.EmptyFluid
import net.minecraft.fluid.Fluid
import net.minecraft.fluid.LavaFluid
import net.minecraft.fluid.WaterFluid

object FluidGenerator : Generator(
    "fluids"
) {
    val REGISTRY = RegistryUtil.getRegistry<Fluid>("FLUID")!!

    override fun generate() {
        for (fluid in FluidGenerator.REGISTRY) {
            val fluidData = compound()
            val resourceLocation = FluidGenerator.REGISTRY.getId(fluid)

            fluidData["id"] = FluidGenerator.REGISTRY.getRawId(fluid)

            ItemGenerator.REGISTRY.getRawId(fluid.bucketItem).let {
                if (it == 0) {
                    return@let
                }
                fluidData["bucket"] = it
            }

            fluid.defaultState

            when (fluid) {
                is LavaFluid -> {
                    fluidData["drip_particle_type"] = ParticleGenerator.REGISTRY.getRawId(fluid.particle?.type)
                }
                is WaterFluid -> {
                    fluidData["drip_particle_type"] = ParticleGenerator.REGISTRY.getRawId(fluid.particle?.type)
                }
                is EmptyFluid -> {
                }
                else -> {
                    TODO("Unknown fluid: $fluid")
                }
            }

            fluidData["class"] = fluid::class.java.realName

            data[resourceLocation.toString()] = fluidData
        }
    }

}
