package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound

object ContainerTypeGenerator : Generator(
    "container_types"
) {
    override fun generate() {
        val registry = RegistryUtil.getRegistry<Any>("SCREEN_HANDLER", "CONTAINER", "MENU")!!
        for (containerType in registry) {
            val resourceLocation = registry.getId(containerType)!!
            val containerTypeData = compound()
            containerTypeData["id"] = registry.getRawId(containerType)



            data[resourceLocation.toString()] = containerTypeData
        }
    }
}
