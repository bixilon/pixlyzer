package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.stat.Stat
import net.minecraft.stat.StatFormatter
import net.minecraft.stat.StatType

object StatisticsGenerator : Generator(
    "statistics"
) {
    val REGISTRY = RegistryUtil.getRegistry<StatType<*>>("STAT_TYPE")!!
    val CUSTOM = RegistryUtil.getRegistry<StatType<*>>("CUSTOM_STAT")!!

    private val registryField = getField(StatType::class.java, "registry")!!

    override fun generate() {
        for (statistic in REGISTRY) {
            val resourceLocation = REGISTRY.getId(statistic)
            val statisticData = compound()
            statisticData["id"] = REGISTRY.getRawId(statistic)


            val registry = registryField.get(statistic)!!

            statisticData["unit"] = when (registry) {
                BlockGenerator.REGISTRY.native -> "block"
                ItemGenerator.REGISTRY.native -> "item"
                EntityGenerator.REGISTRY.native -> "entity_type"
                CUSTOM.native -> "custom"
                StatFormatter.DECIMAL_FORMAT -> "decimal_format"
                StatFormatter.DEFAULT -> "default"
                StatFormatter.DIVIDE_BY_TEN -> "divide_by_ten"
                StatFormatter.DISTANCE -> "distance"
                StatFormatter.TIME -> "time"
                else -> TODO("Can not find unit ${registry}")
            }

            val sortedStatistics: MutableSet<String> = mutableSetOf()
            for (subStatistic in statistic) {
                val key = when (subStatistic) {
                    is Stat -> subStatistic.name
                    else -> subStatistic.toString()
                }
                sortedStatistics.add(key)
            }

            if (sortedStatistics.size > 0) {
                statisticData["sub_statistics"] = sortedStatistics.toSortedSet()
            }

            data[resourceLocation.toString()] = statisticData
        }
    }
}
