package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.block.Block
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.util.Identifier

object BlockEntityGenerator : Generator(
    "block_entities"
) {
    val REGISTRY = RegistryUtil.getRegistry<BlockEntityType<*>>("BLOCK_ENTITY_TYPE", "BLOCK_ENTITY")!!

    override fun generate() {
        for ((blockEntityType, id, resourceLocation) in getBlockEntities()) {
            val blockEntityData = compound()
            blockEntityData["id"] = id

            (BLOCK_ENTITY_VALID_BLOCKS_FIELD?.get(blockEntityType) as Set<Block>?)?.let {
                val blockTypes = mutableListOf<Int>()

                for (block in it) {
                    blockTypes.add(BlockGenerator.REGISTRY.getRawId(block))
                }

                if (blockTypes.size > 0) {
                    blockEntityData["blocks"] = blockTypes.toSortedSet()
                }
            }

            data[resourceLocation.toString()] = blockEntityData
        }
    }

    private val BLOCK_ENTITY_VALID_BLOCKS_FIELD = getField(BlockEntityType::class.java, "blocks")


    private fun getBlockEntities(): Set<Triple<BlockEntityType<*>, Int, Identifier>> {

        val ret: MutableSet<Triple<BlockEntityType<*>, Int, Identifier>> = mutableSetOf()

        for (value in REGISTRY) {
            ret.add(Triple(value as BlockEntityType<*>, REGISTRY.getRawId(value), REGISTRY.getId(value)!!))
        }

        return ret
    }
}
