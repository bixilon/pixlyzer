package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound

object ArgumentTypeGenerator : Generator(
    "argument_type",
    allowEmpty = true,
) {
    val REGISTRY = RegistryUtil.getRegistry<Any>("COMMAND_ARGUMENT_TYPE")

    override fun generate() {
        for (type in REGISTRY ?: return) {
            val data = compound()
            data["id"] = REGISTRY.getRawId(type)
            this.data[REGISTRY.getId(type).toString()] = data
        }
    }
}

