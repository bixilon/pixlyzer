package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.util.math.Box
import net.minecraft.util.shape.VoxelShape

object VoxelShapeGenerator : Generator(
    "shapes"
) {
    private val AABBS: MutableMap<Box, Int> = mutableMapOf()

    override fun generate() {
        val shapes = mutableListOf<Any>()
        val shapeList: MutableList<Any> = mutableListOf()

        for ((voxelShape, id) in BlockGenerator.VOXEL_SHAPES) {
            shapeList.add(id, voxelShape)
        }
        for (voxelShape in shapeList) {
            shapes.add(voxelShape)
        }
        data["shapes"] = shapes


        val aabbs = mutableListOf<Any>()
        val aabbList: MutableList<Box> = mutableListOf()
        for ((aabb, index) in AABBS) {
            aabbList.add(index, aabb)
        }
        for (aabb in aabbList) {
            aabbs += aabb.serialize()
        }
        data["aabbs"] = aabbs
    }


    fun VoxelShape.serialize(): Any {
        val json = mutableListOf<Any>()
        forEachBox { x1, y1, z1, x2, y2, z2 ->
            val aabb = Box(x1, y1, z1, x2, y2, z2)
            val index = AABBS.getOrPut(aabb) { AABBS.size }
            json.add(index)
        }
        if (json.size == 1) {
            return json[0]
        }
        return json
    }

    private fun Box.serialize(): MutableMap<Any, Any> {
        val minX = AABB_MIN_X_FIELD.getDouble(this)
        val minY = AABB_MIN_Y_FIELD.getDouble(this)
        val minZ = AABB_MIN_Z_FIELD.getDouble(this)

        val maxX = AABB_MAX_X_FIELD.getDouble(this)
        val maxY = AABB_MAX_Y_FIELD.getDouble(this)
        val maxZ = AABB_MAX_Z_FIELD.getDouble(this)
        val json = compound()

        val from = mutableListOf<Any>()
        from.add(minX)
        from.add(minY)
        from.add(minZ)
        if (minX == minY && minY == minZ) {
            json["from"] = minX
        } else {
            json["from"] = from
        }

        val to = mutableListOf<Any>()
        to.add(maxX)
        to.add(maxY)
        to.add(maxZ)

        if (maxX == maxY && maxY == maxZ) {
            json["to"] = maxX
        } else {
            json["to"] = to
        }

        return json
    }

    private val AABB_MIN_X_FIELD = getField(Box::class.java, "minX", "x1")!!
    private val AABB_MIN_Y_FIELD = getField(Box::class.java, "minY", "y1")!!
    private val AABB_MIN_Z_FIELD = getField(Box::class.java, "minZ", "z1")!!

    private val AABB_MAX_X_FIELD = getField(Box::class.java, "maxX", "x2")!!
    private val AABB_MAX_Y_FIELD = getField(Box::class.java, "maxY", "y2")!!
    private val AABB_MAX_Z_FIELD = getField(Box::class.java, "maxZ", "z2")!!

}
