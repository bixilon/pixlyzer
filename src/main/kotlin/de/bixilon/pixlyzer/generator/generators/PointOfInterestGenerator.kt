package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.exceptions.GeneratorSkipException
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import java.lang.reflect.Method

object PointOfInterestGenerator : Generator(
    "points_of_interest"
) {
    val REGISTRY = RegistryUtil.getRegistry<Any>("POINT_OF_INTEREST_TYPE")

    override fun generate() {
        if (REGISTRY == null) {
            throw GeneratorSkipException("Not available in this version yet!")
        }

        for (pointOfInterestType in REGISTRY) {
            val resourceLocation = REGISTRY.getId(pointOfInterestType)!!
            val pointOfInterestData = compound()
            pointOfInterestData["id"] = REGISTRY.getRawId(pointOfInterestType)
            // ToDo: pointOfInterestData["ticket_count"] =  pointOfInterestType.ticketCount
            SEARCH_DISTANCE_METHOD?.invoke(pointOfInterestType)?.let {
                pointOfInterestData["search_distance"] = it as Int
            }

            (POINT_OF_INTEREST_MATCHING_STATES_FIELD?.get(pointOfInterestType) as Set<BlockState>?)?.let {
                val states = mutableListOf<Int>()
                for (state in it) {
                    states.add(Block.getRawIdFromState(state))
                }
                pointOfInterestData["matching_states"] = states.toSortedSet()
            }

            data[resourceLocation.toString()] = pointOfInterestData
        }
    }


    val POINT_OF_INTEREST_CLASS = getClass("net.minecraft.world.poi.PointOfInterestType", "net.minecraft.village.PointOfInterestType")

    private val POINT_OF_INTEREST_MATCHING_STATES_FIELD = getField(POINT_OF_INTEREST_CLASS, "blockStates")

    private val SEARCH_DISTANCE_METHOD: Method? = try {
        POINT_OF_INTEREST_CLASS?.getDeclaredMethod("method_21648")
    } catch (exception: Exception) {
        try {
            POINT_OF_INTEREST_CLASS?.getDeclaredMethod("getSearchDistance")
        } catch (exception: Exception) {
            null
        }
    }

}
