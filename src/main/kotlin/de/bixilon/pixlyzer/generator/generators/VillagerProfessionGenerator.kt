package de.bixilon.pixlyzer.generator.generators

import com.google.common.collect.ImmutableSet
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.block.Block
import net.minecraft.item.Item
import net.minecraft.sound.SoundEvent
import net.minecraft.village.VillagerProfession

object VillagerProfessionGenerator : Generator(
    "villager_professions"
) {
    val REGISTRY = RegistryUtil.getRegistry<VillagerProfession>("VILLAGER_PROFESSION")!!

    override fun generate() {
        for (villagerProfession in REGISTRY) {
            val resourceLocation = REGISTRY.getId(villagerProfession)
            val villagerProfessionData = compound()
            villagerProfessionData["id"] = REGISTRY.getRawId(villagerProfession)

            PointOfInterestGenerator.REGISTRY?.let {
                villagerProfessionData["work_station"] = it.getRawId(WORK_STATION_VILLAGER_PROFESSION_TYPE_FIELD!!.get(villagerProfession))
            }

            (GATHERABLE_ITEMS_FIELD?.get(villagerProfession) as ImmutableSet<Item>?)?.let {
                val requestedItems = mutableListOf<Int>()
                for (item in it) {
                    requestedItems.add(ItemGenerator.REGISTRY.getRawId(item))
                }

                if (requestedItems.size > 0) {
                    villagerProfessionData["requested_items"] = requestedItems.toSortedSet()
                }
            }

            (SECONDARY_JOB_SITES_FIELD?.get(villagerProfession) as ImmutableSet<Block>?)?.let {
                val blocks = mutableListOf<Int>()
                for (block in it) {
                    blocks.add(BlockGenerator.REGISTRY.getRawId(block))
                }

                if (blocks.size > 0) {
                    villagerProfessionData["blocks"] = blocks.toSortedSet()
                }
            }

            (WORK_SOUND_PROFESSION?.get(villagerProfession) as SoundEvent?)?.let {
                villagerProfessionData["work_sound"] = SoundEventGenerator.REGISTRY.getRawId(it)
            }

            data[resourceLocation.toString()] = villagerProfessionData
        }
    }

    private val WORK_SOUND_PROFESSION = getField(VillagerProfession::class.java, "workSound")


    private val WORK_STATION_VILLAGER_PROFESSION_TYPE_FIELD = getField(VillagerProfession::class.java, "workStation", "heldWorkstation")

    private val SECONDARY_JOB_SITES_FIELD = getField(VillagerProfession::class.java, "secondaryJobSites")
    private val GATHERABLE_ITEMS_FIELD = getField(VillagerProfession::class.java, "gatherableItems")
}
