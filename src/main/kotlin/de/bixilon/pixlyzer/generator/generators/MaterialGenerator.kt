package de.bixilon.pixlyzer.generator.generators

import com.google.common.collect.HashBiMap
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.util.Identifier
import java.lang.reflect.Modifier
import java.util.*

object MaterialGenerator : Generator(
    "materials",
    allowEmpty = true,
) {
    override fun generate() {
        for ((resourceLocation, material) in MATERIALS) {
            val materialData = compound()
            val color = MATERIAL_GET_COLOR_METHOD!!.invoke(material)
            val colorColor = color::class.java.getDeclaredField("color").getInt(color)
            materialData["color"] = colorColor
            SOFT?.let { materialData["is_soft"] = it.getBoolean(material) }
            BLOCK_MOTION?.let { materialData["blocks_motion"] = it.getBoolean(material) }
            BLOCKS_LIGHT?.let { materialData["solid_blocking"] = it.getBoolean(material) }
            REPLACEABLE?.let { materialData["replaceable"] = it.getBoolean(material) }
            SOLID?.let { materialData["solid"] = it.getBoolean(material) }


            data[resourceLocation.toString()] = materialData
        }
    }


    val CLASS = getClass("net.minecraft.block.Material")
    private val MATERIAL_GET_COLOR_METHOD = CLASS?.getDeclaredMethod("getColor")

    private val SOFT = getField(CLASS, "breakByHand")
    private val BLOCK_MOTION = getField(CLASS, "blocksMovement")
    private val REPLACEABLE = getField(CLASS, "replaceable")
    private val SOLID = getField(CLASS, "isSolid")
    private val BLOCKS_LIGHT = getField(CLASS, "blocksLight")

    private fun getMaterials(): HashBiMap<Identifier, Any> {
        if (CLASS == null) return HashBiMap.create()
        val materials: HashBiMap<Identifier, Any> = HashBiMap.create()
        for (field in CLASS.declaredFields) {
            if (field.type.name != CLASS.name) {
                continue
            }
            if (!Modifier.isStatic(field.modifiers)) {
                continue
            }

            field.isAccessible = true
            materials[Identifier(field.name.lowercase(Locale.getDefault()))] = field.get(null)
        }

        return materials
    }


    val MATERIALS = getMaterials()
}
