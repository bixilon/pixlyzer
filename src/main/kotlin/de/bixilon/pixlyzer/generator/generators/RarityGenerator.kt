package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.util.Rarity

object RarityGenerator : Generator(
    "rarities"
) {
    override fun generate() {
        for (rarity in Rarity.values()) {
            val rarityData = compound()
            rarityData["name"] = rarity.name.toLowerCase()
            rarityData["color"] = rarity.formatting.name.toLowerCase()

            data[rarity.ordinal] = rarityData
        }
    }
}
