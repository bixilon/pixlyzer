package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.sound.SoundEvent

object SoundEventGenerator : Generator(
    "sound_events"
) {
    override fun generate() {
        for (soundEvent in REGISTRY) {
            val resourceLocation = REGISTRY.getId(soundEvent)
            val soundEventData = compound()
            soundEventData["id"] = REGISTRY.getRawId(soundEvent)


            data[resourceLocation.toString()] = soundEventData
        }
    }

    val REGISTRY = RegistryUtil.getRegistry<SoundEvent>("SOUND_EVENT")!!
}
