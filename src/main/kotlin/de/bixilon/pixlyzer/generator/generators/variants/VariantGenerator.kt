package de.bixilon.pixlyzer.generator.generators.variants

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util

abstract class VariantGenerator(
    name: String,
    val clazz: Class<Any>?,
    val registry: RegistryUtil.PixLyzerRegistry<Any>?,
) : Generator("variant/$name", allowEmpty = clazz == null) {
    private val textureField = if (clazz == null) null else getField(clazz, "texture", "textureId")!!

    open fun process(data: MutableMap<Any, Any>, item: Any) = Unit

    override fun generate() {
        if (registry == null || textureField == null) {
            return
        }
        for (item in registry) {
            val resourceLocation = registry.getId(item)
            val data = Util.compound()
            data["id"] = registry.getRawId(item)

            data["texture"] = textureField.get(item).toString()

            process(data, item)

            this.data[resourceLocation.toString()] = data
        }
    }
}
