package de.bixilon.pixlyzer.generator.generators

import com.mojang.bridge.game.GameVersion
import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.MinecraftVersion

object VersionGenerator : Generator(
    "version",
    allowedFail = true,
) {
    override fun generate() {
        val version = MinecraftVersion::class.java.getDeclaredMethod("create").invoke(null) as GameVersion

        data["id"] = version.id
        data["name"] = version.name
        data["stable"] = version.isStable
        data["world_version"] = version.worldVersion
        data["protocol_version"] = version.protocolVersion
        data["build_time"] = version.buildTime.toString()
    }
}
