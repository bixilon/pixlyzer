package de.bixilon.pixlyzer.generator.generators.variants

import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.RegistryUtil

object FrogVariantGenerator : VariantGenerator(
    "frog",
    getClass("net.minecraft.entity.passive.FrogVariant"),
    RegistryUtil.getRegistry("FROG_VARIANT"),
)
