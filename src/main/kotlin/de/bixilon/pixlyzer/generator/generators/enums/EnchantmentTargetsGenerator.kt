package de.bixilon.pixlyzer.generator.generators.enums

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.enchantment.EnchantmentTarget

object EnchantmentTargetsGenerator : Generator(
    "enchantment_targets"
) {
    override fun generate() {
        for (target in EnchantmentTarget.values()) {
            val enchantmentTargetData = compound()

            enchantmentTargetData["name"] = target.name

            data[target.ordinal.toString()] = enchantmentTargetData

        }
    }
}
