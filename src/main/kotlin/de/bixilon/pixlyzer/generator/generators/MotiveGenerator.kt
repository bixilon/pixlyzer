package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound

object MotiveGenerator : Generator(
    "motives"
) {
    val REGISTRY = RegistryUtil.getRegistry<Any>("PAINTING_VARIANT", "PAINTING_MOTIVE", "MOTIVE")!!

    override fun generate() {
        for (motive in REGISTRY) {
            val resourceLocation = REGISTRY.getId(motive)
            val motiveData = compound()
            motiveData["id"] = REGISTRY.getRawId(motive)

            motiveData["width"] = WIDTH_FIELD.get(motive) as Int
            motiveData["height"] = HEIGHT_FIELD.get(motive) as Int

            data[resourceLocation.toString()] = motiveData
        }
    }

    val MOTIF_CLASS = getClass("net.minecraft.entity.decoration.painting.PaintingVariant", "net.minecraft.entity.decoration.painting.PaintingMotive")
    val WIDTH_FIELD = getField(MOTIF_CLASS, "width")!!
    val HEIGHT_FIELD = getField(MOTIF_CLASS, "height")!!
}
