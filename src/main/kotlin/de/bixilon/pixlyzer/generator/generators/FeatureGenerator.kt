package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.util.Identifier

object FeatureGenerator : Generator(
    "features",
    allowEmpty = true,
) {
    private val FEATURE_MANAGER = getField(getClass("net.minecraft.resource.featuretoggle.FeatureFlags"), "FEATURE_MANAGER")?.get(null)
    private val FEATURE_SET = getClass("net.minecraft.resource.featuretoggle.FeatureSet")
    private val FEATURE_SET_UNIVERSE = getField(FEATURE_SET, "universe")
    private val FEATURE_SET_MASK = getField(FEATURE_SET, "featuresMask")

    override fun generate() {
        if (FEATURE_MANAGER == null) {
            return
        }
        val map = (getField(FEATURE_MANAGER::class.java, "featureFlags")!!.get(FEATURE_MANAGER) as Map<Identifier, Any>).entries.toList().sortedBy { it.key.toString() }

        var index = 0
        for ((identifier, flag) in map) {
            val data = compound()
            data["id"] = index++
            data["mask"] = getField(flag::class.java, "mask")!!.getLong(flag)
            data["universe"] = getField(flag::class.java, "universe")!!.get(flag).toString()
            this.data[identifier.toString()] = data
        }
    }

    fun Any?.serializeFeatureSet(): Any? {
        if (this == null) {
            return null
        }
        val mask = FEATURE_SET_MASK!!.getLong(this)
        if (mask == 0L) {
            return null
        }
        val universe = FEATURE_SET_UNIVERSE!!.get(this).toString()
        if (universe == "main") {
            return null
        }

        return mapOf(
            "universe" to universe,
            "mask" to mask,
        )
    }
}

