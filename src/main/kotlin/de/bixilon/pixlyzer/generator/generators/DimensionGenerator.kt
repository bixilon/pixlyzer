package de.bixilon.pixlyzer.generator.generators

import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.ReflectionUtil.getGetter
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import de.bixilon.pixlyzer.util.Util.mapCast
import net.minecraft.util.Identifier
import net.minecraft.world.dimension.DimensionType
import java.lang.reflect.Modifier
import java.util.*

object DimensionGenerator : Generator(
    "dimensions",
    allowEmpty = true,
) {
    override fun generate() {
        for ((resourceLocation, id, dimension) in getDimensions()) {
            val dimensionData = compound()

            id?.let {
                dimensionData["id"] = id
            }


            SUFFIX_FUNCTION?.let { dimensionData["file_suffix"] = it.invoke(dimension) }

            (FIXED_TIME_FIELD?.get(dimension) as OptionalLong?)?.let {
                if (it.isPresent) {
                    dimensionData["fixed_time"] = it.asLong
                }
            }
            HAS_SKYLIGHT_FIELD.let {
                dimensionData["has_sky_light"] = it.getBoolean(dimension)
            }
            HAS_CEILING_FIELD?.let {
                dimensionData["has_ceiling"] = it.getBoolean(dimension)
            }
            ULTRA_WARM_FIELD?.let {
                dimensionData["ultra_warm"] = it.getBoolean(dimension)
            }
            NATURAL_FIELD?.let {
                dimensionData["natural"] = it.getBoolean(dimension)
            }
            COORDINATE_SCALE_FIELD?.let {
                dimensionData["coordinate_scale"] = it.getDouble(dimension)
            }
            CREATE_DRAGON_FIGHT_FIELD?.let {
                dimensionData["create_dragon_fight"] = it.getBoolean(dimension)
            }
            PIGLIN_SAFE_FIELD?.let {
                dimensionData["piglin_safe"] = it.getBoolean(dimension)
            }
            BED_WORKS_FIELD?.let {
                dimensionData["bed_works"] = it.getBoolean(dimension)
            }
            RESPAWN_ANCHOR_WORKS_FIELD?.let {
                dimensionData["respawn_anchor_works"] = it.getBoolean(dimension)
            }
            HAS_RAIDS_FIELD?.let {
                dimensionData["has_raids"] = it.getBoolean(dimension)
            }
            MIN_Y_FIELD?.let {
                dimensionData["minimum_y"] = it.getInt(dimension)
            }
            HEIGHT_FIELD?.let {
                dimensionData["height"] = it.getInt(dimension)
            }
            LOGICAL_HEIGHT_FIELD?.let {
                dimensionData["logical_height"] = it.getInt(dimension)
            }
            INFINIBURN_FIELD?.let {
                dimensionData["infiniburn"] = it.get(dimension).tagKey().toString()
            }
            EFFECTS_LOCATION_FIELD?.let {
                dimensionData["effects_location"] = (it.get(dimension) as Identifier).toString()
            }
            AMBIENT_LIGHT_FIELD?.let {
                dimensionData["ambient_light"] = it.getFloat(dimension)
            }

            DIMENSION_BIOME_ZOOMER_FIELD?.get(dimension)?.let {
                when (it::class.java.simpleName) {
                    "VoronoiBiomeAccessType" -> {
                        dimensionData["supports_3d_biomes"] = true
                    }

                    "HorizontalVoronoiBiomeAccessType" -> {
                        dimensionData["supports_3d_biomes"] = false
                    }

                    else -> {
                        TODO("Can not check if dimension $resourceLocation supports 3d biomes!")
                    }
                }
            }


            data[resourceLocation.toString()] = dimensionData
        }
    }

    private val DIMENSION_TYPE_CLASS = DimensionType::class.java

    private val FIXED_TIME_FIELD = getField(DIMENSION_TYPE_CLASS, "fixedTime")
    private val HAS_SKYLIGHT_FIELD = getField(DIMENSION_TYPE_CLASS, "hasSkylight", "hasSkyLight")!!
    private val HAS_CEILING_FIELD = getField(DIMENSION_TYPE_CLASS, "hasCeiling")
    private val ULTRA_WARM_FIELD = getField(DIMENSION_TYPE_CLASS, "ultraWarm")
    private val NATURAL_FIELD = getField(DIMENSION_TYPE_CLASS, "natural")
    private val COORDINATE_SCALE_FIELD = getField(DIMENSION_TYPE_CLASS, "coordinateScale")
    private val CREATE_DRAGON_FIGHT_FIELD = getField(DIMENSION_TYPE_CLASS, "createDragonFight")
    private val PIGLIN_SAFE_FIELD = getField(DIMENSION_TYPE_CLASS, "piglinSafe")
    private val BED_WORKS_FIELD = getField(DIMENSION_TYPE_CLASS, "bedWorks")
    private val RESPAWN_ANCHOR_WORKS_FIELD = getField(DIMENSION_TYPE_CLASS, "respawnAnchorWorks")
    private val HAS_RAIDS_FIELD = getField(DIMENSION_TYPE_CLASS, "hasRaids")
    private val MIN_Y_FIELD = getField(DIMENSION_TYPE_CLASS, "minY", "minimumY")
    private val HEIGHT_FIELD = getField(DIMENSION_TYPE_CLASS, "height")
    private val LOGICAL_HEIGHT_FIELD = getField(DIMENSION_TYPE_CLASS, "logicalHeight")
    private val INFINIBURN_FIELD = getField(DIMENSION_TYPE_CLASS, "infiniburn")
    private val EFFECTS_LOCATION_FIELD = getField(DIMENSION_TYPE_CLASS, "effectsLocation")
    private val AMBIENT_LIGHT_FIELD = getField(DIMENSION_TYPE_CLASS, "ambientLight")


    private val SUFFIX_FUNCTION = getGetter(DimensionType::class.java, "getSuffix")


    private val RESOURCE_KEY_CLASS = getClass("net.minecraft.resources.ResourceKey", "net.minecraft.util.registry.RegistryKey")

    private val RESOURCE_KEY_LOCATION_METHOD = getField(RESOURCE_KEY_CLASS, "value", "location")

    private val DIMENSION_BIOME_ZOOMER_FIELD = getField(DimensionType::class.java, "biomeAccessType")


    private fun getDimensions(): MutableSet<Triple<Identifier, Int?, DimensionType>> {
        val types: MutableSet<Triple<Identifier, Int?, DimensionType>> = mutableSetOf()


        val dimensionRegistry = RegistryUtil.getRegistry<Any>("DIMENSION", "DIMENSION_TYPE")

        try {
            dimensionRegistry?.let {
                val idMethod1 = try {
                    DimensionType::class.java.getDeclaredMethod("getRawId")
                } catch (exception: Throwable) {
                    null
                }
                val idMethod2 = try {
                    it::class.java.getDeclaredMethod("getRawId", Object::class.java)
                } catch (exception: Throwable) {
                    null
                }
                for (entry in it) {
                    check(entry is DimensionType)
                    val id1 = idMethod1?.invoke(entry) ?: idMethod2?.invoke(it, entry) ?: throw IllegalStateException("Can not get dimension id for $entry")
                    types.add(Triple(dimensionRegistry.getId(entry.unsafeCast())!!, id1 as Int, entry))
                }
            }
        } catch (exception: Exception) {
        }

        if (types.isEmpty()) {
            for (field in DimensionType::class.java.declaredFields) {
                if (field.type != DimensionType::class.java) {
                    continue
                }
                if (!Modifier.isStatic(field.modifiers)) {
                    continue
                }
                field.isAccessible = true
                val resourceLocation = when (field.name) {
                    "OVERWORLD", "OVERLORD", "field_25407" -> Identifier("overworld")
                    "OVERWORLD_CAVES" -> Identifier("overworld_caves")
                    "THE_NETHER", "_NETHER" -> Identifier("the_nether")
                    "THE_END", "_END" -> Identifier("the_end")
                    else -> TODO("Can not find dimension ${field.name}")
                }
                types.add(Triple(resourceLocation, null, field.get(null) as DimensionType))
            }
        }
        if (types.isEmpty()) {
            val field = getField(DimensionType::class.java, "field_24759", "BUILTIN") ?: return types

            for ((resourceKey, dimension) in field.get(null) as Map<Any, DimensionType>) {
                types.add(Triple(RESOURCE_KEY_LOCATION_METHOD!!.get(resourceKey) as Identifier, null, dimension))
            }
        }

        return types
    }

    private val TAG_KEY_CLASS = getClass("net.minecraft.tag.TagKey")
    private val TAG_KEY_ID_FIELD = getField(TAG_KEY_CLASS, "id")

    private fun Any.tagKey(): Identifier {
        if (this is Identifier) {
            return this
        }
        if (this::class.java == TAG_KEY_CLASS) {
            return TAG_KEY_ID_FIELD!!.get(this) as Identifier
        }
        TODO("Unknown identifier: $this")
    }


    override fun test() {
        check(data["minecraft:overworld"]!!.mapCast()!!["has_sky_light"] == true)
        check(data["minecraft:the_nether"]!!.mapCast()!!["has_sky_light"] == false)
        check(data["minecraft:the_end"]!!.mapCast()!!["has_sky_light"] == false)
    }
}
