package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.Util.nullCast
import net.minecraft.entity.data.TrackedDataHandler
import net.minecraft.entity.data.TrackedDataHandlerRegistry
import java.lang.reflect.Modifier

object EntityDataDataTypeGenerator : Generator(
    "entity_data_data_types"
) {
    override fun generate() {
        val clazz = TrackedDataHandlerRegistry::class.java

        val biMapClass = getClass("net.minecraft.util.collection.Int2ObjectBiMap", "net.minecraft.util.Int2ObjectBiMap")!!
        val idMethod = try {
            biMapClass.getDeclaredMethod("getRawId", Any::class.java)
        } catch (exception: Throwable) {
            biMapClass.getDeclaredMethod("getId", Any::class.java)
        }
        val handlers = getField(clazz, "DATA_HANDLERS", "field_13328")!!.get(null)

        val data: MutableMap<Int, Any> = mutableMapOf()

        for (field in clazz.declaredFields) {
            if (!Modifier.isStatic(field.modifiers)) {
                continue
            }
            if (!TrackedDataHandler::class.java.isAssignableFrom(field.type)) {
                continue
            }
            val name = field.name.correct()
            val id = idMethod.invoke(handlers, field.get(null).nullCast()) as Int

            data[id] = name
        }
        this.data.putAll(data.toSortedMap())
    }

    private fun String.correct() = when (this) {
        "BYTE" -> "BYTE"
        "INTEGER" -> "INTEGER"
        "FLOAT" -> "FLOAT"
        "STRING" -> "STRING"
        "TEXT_COMPONENT" -> "TEXT_COMPONENT"
        "OPTIONAL_TEXT_COMPONENT" -> "OPTIONAL_TEXT_COMPONENT"
        "ITEM_STACK" -> "ITEM_STACK"
        "OPTIONAL_BLOCK_STATE" -> "OPTIONAL_BLOCK_STATE"
        "BOOLEAN" -> "BOOLEAN"
        "PARTICLE" -> "PARTICLE"
        "ROTATION" -> "ROTATION"
        "BLOCK_POS" -> "VEC3I"
        "OPTIONAL_BLOCK_POS", "OPTIONA_BLOCK_POS" -> "OPTIONAL_VEC3I"
        "FACING" -> "DIRECTION"
        "OPTIONAL_UUID" -> "OPTIONAL_UUID"
        "field_38825" -> "GLOBAL_POSITION"
        "OPTIONAL_GLOBAL_POS" -> "OPTIONAL_GLOBAL_POSITION"
        "NBT_COMPOUND", "TAG_COMPOUND" -> "NBT"
        "VILLAGER_DATA" -> "VILLAGER_DATA"
        "OPTIONAL_INT" -> "OPTIONAL_INTEGER"
        "ENTITY_POSE" -> "POSE"
        "field_38826", "CAT_VARIANT" -> "CAT_VARIANT"
        "field_38827", "FROG_VARIANT" -> "FROG_VARIANT"
        "FIREWORK_DATA", "field_17910" -> "FIREWORK_DATA"
        "field_39017", "PAINTING_VARIANT" -> "MOTIF"
        "LONG" -> "LONG"
        "BLOCK_STATE" -> "BLOCK_STATE"
        "VECTOR3F" -> "VEC3F"
        "QUATERNIONF" -> "QUATERNIONF"
        "SNIFFER_STATE" -> "SNIFFER_STATE"
        "TRANSFORMATION" -> "TRANSFORMATION" // 23w13a_or_b
        else -> TODO("Unknown meta data type: $this")
    }
}
