package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.sound.BlockSoundGroup
import java.lang.reflect.Modifier

object SoundGroupGenerator : Generator(
    "sound_groups",
) {
    private val SOUND_GROUP_CLASS = getClass("net.minecraft.sound.BlockSoundGroup")
    private var ids: MutableMap<BlockSoundGroup, Int> = mutableMapOf()

    override fun generate() {
        val fields = SOUND_GROUP_CLASS?.fields ?: return
        for (field in fields) {
            if (!Modifier.isStatic(field.modifiers)) {
                continue
            }
            val data = compound()
            val group = field.get(null)
            if (group !is BlockSoundGroup) {
                continue
            }
            val id = ids.size
            ids[group] = id

            if (group.volume != 1.0f) {
                data["sound_type_volume"] = group.volume
            }
            if (group.volume != 1.0f) {
                data["sound_type_pitch"] = group.pitch
            }
            data["name"] = field.name.lowercase()
            data["break_sound_type"] = SoundEventGenerator.REGISTRY.getRawId(group.breakSound)
            data["step_sound_type"] = SoundEventGenerator.REGISTRY.getRawId(group.stepSound)
            data["place_sound_type"] = SoundEventGenerator.REGISTRY.getRawId(group.placeSound)
            data["hit_sound_type"] = SoundEventGenerator.REGISTRY.getRawId(group.hitSound)
            data["fall_sound_type"] = SoundEventGenerator.REGISTRY.getRawId(group.fallSound)

            this.data[id] = data
        }
    }


    fun getId(sound: BlockSoundGroup): Int {
        return ids[sound]!!
    }
}
