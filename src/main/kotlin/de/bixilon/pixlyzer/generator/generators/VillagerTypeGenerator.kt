package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.village.VillagerType

object VillagerTypeGenerator : Generator(
    "villager_types"
) {
    val REGISTRY = RegistryUtil.getRegistry<VillagerType>("VILLAGER_TYPE")!!

    override fun generate() {
        for (villagerType in REGISTRY) {
            val resourceLocation = REGISTRY.getId(villagerType)
            val villagerTypeData = compound()
            villagerTypeData["id"] = REGISTRY.getRawId(villagerType)

            data[resourceLocation.toString()] = villagerTypeData
        }
    }
}
