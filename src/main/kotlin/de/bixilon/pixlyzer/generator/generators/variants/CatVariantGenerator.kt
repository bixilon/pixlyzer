package de.bixilon.pixlyzer.generator.generators.variants

import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.RegistryUtil

object CatVariantGenerator : VariantGenerator(
    "cat",
    getClass("net.minecraft.entity.passive.CatVariant"),
    RegistryUtil.getRegistry("CAT_VARIANT"),
)
