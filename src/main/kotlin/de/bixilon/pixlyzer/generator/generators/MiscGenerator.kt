package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.ReflectionUtil.variable
import de.bixilon.pixlyzer.util.Util.nullCast
import net.minecraft.block.Blocks
import net.minecraft.block.FireBlock
import net.minecraft.block.entity.AbstractFurnaceBlockEntity

object MiscGenerator : Generator(
    "misc"
) {

    private fun generateBurnChances() {
        "fire_burn_chances".add {
            for ((block, chance) in Blocks.FIRE.nullCast<FireBlock>()!!.burnChances) {
                it[BlockGenerator.REGISTRY.getRawId(block)] = chance
            }
        }
    }

    private fun generateSpreadChances() {
        "fire_spread_chances".add {
            for ((block, chance) in Blocks.FIRE.nullCast<FireBlock>()!!.spreadChances) {
                it[BlockGenerator.REGISTRY.getRawId(block)] = chance
            }
        }
    }

    private fun generateFuelTime() {
        "fuel_time".add {
            for ((block, fuelTime) in AbstractFurnaceBlockEntity.createFuelTimeMap()) {
                it[ItemGenerator.REGISTRY.getRawId(block)] = fuelTime
            }
        }
    }

    private fun generateMapColor() {
        val colors = IntArray(64)
        val clazz = getClass("net.minecraft.block.MapColor", "net.minecraft.block.MaterialColor")!!
        val field = getField(clazz, "COLORS")!!
        for ((index, color) in (field.get(null) as Array<Any?>).withIndex()) {
            colors[index] = color?.variable<Int>("color") ?: -1
        }
        this.data["map_color"] = colors.toTypedArray()
    }

    override fun generate() {
        generateBurnChances()
        generateSpreadChances()
        generateFuelTime()
        generateMapColor()
    }


    fun String.add(generator: (json: MutableMap<Any, Any>) -> Unit) {
        val json: MutableMap<Any, Any> = mutableMapOf()
        try {
            generator(json)
        } catch (exception: Throwable) {
            throw Exception("Misc generator failed in $this", exception)
        }
        data[this] = json.toSortedMap { a, b ->
            if (a is Int && b is Int) {
                return@toSortedMap a - b
            }
            return@toSortedMap 0
        }
    }
}
