package de.bixilon.pixlyzer.generator.generators

import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.entity.attribute.EntityAttribute
import net.minecraft.entity.attribute.EntityAttributeModifier
import net.minecraft.entity.effect.StatusEffect
import java.lang.reflect.Field

object StatusEffectGenerator : Generator(
    "status_effects"
) {
    val REGISTRY = RegistryUtil.getRegistry<StatusEffect>("STATUS_EFFECT", "MOB_EFFECT")!!

    override fun generate() {
        for (mobEffect in REGISTRY) {
            val resourceLocation = REGISTRY.getId(mobEffect)
            val mobEffectData = compound()
            mobEffectData["id"] = REGISTRY.getRawId(mobEffect)

            // ToDO: Was renamed from name to category mobEffectData["category"] = mobEffect.type.name.toLowerCase() // ToDo: add category color
            mobEffectData["color"] = mobEffect.color

            mobEffect.getTranslationKey()?.let {
                mobEffectData["translation_key"] = it
            }
            val modifiers = compound()

            for ((attribute, modifier) in mobEffect.getModifiers()) {
                val value = if (modifier is EntityAttributeModifier) modifier else CREATOR_CREATE!!.invoke(modifier, 1) as EntityAttributeModifier
                val modifierData = compound()
                modifierData["name"] = value.name.toLowerCase()
                modifierData["uuid"] = value.id.toString()
                modifierData["amount"] = AMOUNT_ENTITY_ATTRIBUTE_MODIFIER_FIELD.getDouble(value)
                modifierData["operation"] = OPERATION_NAME.get(value.operation).unsafeCast<String>().toLowerCase()

                modifiers[EntityGenerator.getKeyFromMobEffect(attribute)] = modifierData
            }

            if (modifiers.isNotEmpty()) {
                mobEffectData["attributes"] = modifiers
            }

            data[resourceLocation.toString()] = mobEffectData
        }
    }

    //     private val MOB_EFFECT_CATEGORY_FIELD: Field = StatusEffect::class.java.getDeclaredField("type")
    private val MOB_EFFECT_MODIFIERS_FIELD: Field = StatusEffect::class.java.getDeclaredField("attributeModifiers")
    private val AMOUNT_ENTITY_ATTRIBUTE_MODIFIER_FIELD = getField(EntityAttributeModifier::class.java, "amount", "value")!!

    private val CREATOR_CLASS = getClass("net.minecraft.entity.attribute.AttributeModifierCreator")
    private val CREATOR_CREATE = CREATOR_CLASS?.getDeclaredMethod("createAttributeModifier", Int::class.java)

    private val OPERATION_NAME = Enum::class.java.getDeclaredField("name")
    init {
        // MOB_EFFECT_CATEGORY_FIELD.isAccessible = true
        MOB_EFFECT_MODIFIERS_FIELD.isAccessible = true
        CREATOR_CREATE?.isAccessible = true
        OPERATION_NAME.isAccessible = true
    }

    // private fun StatusEffect.getCategory(): StatusEffectType {
    //     return MOB_EFFECT_CATEGORY_FIELD.get(this) as StatusEffectType
    // }

    private fun StatusEffect.getModifiers(): Map<EntityAttribute, Any> {
        return MOB_EFFECT_MODIFIERS_FIELD.get(this) as Map<EntityAttribute, Any>
    }
}
