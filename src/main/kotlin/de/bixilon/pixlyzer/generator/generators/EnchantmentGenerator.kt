package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.enchantment.Enchantment
import net.minecraft.entity.EquipmentSlot
import java.lang.reflect.Field

object EnchantmentGenerator : Generator(
    "enchantments"
) {
    val REGISTRY = RegistryUtil.getRegistry<Enchantment>("ENCHANTMENT")!!

    override fun generate() {
        for (enchantment in REGISTRY) {
            val resourceLocation = REGISTRY.getId(enchantment)
            val enchantmentData = compound()

            enchantmentData["id"] = REGISTRY.getRawId(enchantment)

            enchantment.getTranslationKey()?.let {
                enchantmentData["translation_key"] = it
            }
            enchantmentData["rarity"] = (ENCHANTMENT_RARITY_FIELD.get(enchantment) as Enum<*>).ordinal

            val equipmentSlots = mutableListOf<Any>()

            for (slot in enchantment.getSlots()) {
                slot?.let {
                    equipmentSlots.add(slot.name.toLowerCase())
                }
            }
            if (equipmentSlots.size > 0) {
                enchantmentData["slots"] = equipmentSlots
            }
            enchantmentData["minimum_level"] = ENCHANTMENT_MINIMUM_LEVEL_METHOD.invoke(enchantment) as Int
            enchantmentData["maximum_level"] = ENCHANTMENT_MAXIMUM_LEVEL_METHOD.invoke(enchantment) as Int

            data[resourceLocation.toString()] = enchantmentData
        }
    }

    private val ENCHANTMENT_SLOTS_FIELD: Field = Enchantment::class.java.getDeclaredField("slotTypes")
    private val ENCHANTMENT_RARITY_FIELD: Field = getField(Enchantment::class.java, "rarity", "weight")!!

    private val ENCHANTMENT_MINIMUM_LEVEL_METHOD = try {
        Enchantment::class.java.getMethod("getMinimumLevel")
    } catch (exception: Exception) {
        Enchantment::class.java.getMethod("getMinLevel")
    }
    private val ENCHANTMENT_MAXIMUM_LEVEL_METHOD = try {
        Enchantment::class.java.getMethod("getMaximumLevel")
    } catch (exception: Exception) {
        Enchantment::class.java.getMethod("getMaxLevel")
    }

    init {
        ENCHANTMENT_SLOTS_FIELD.isAccessible = true
    }

    private fun Enchantment.getSlots(): Array<EquipmentSlot?> {
        return ENCHANTMENT_SLOTS_FIELD.get(this) as Array<EquipmentSlot?>
    }

}

