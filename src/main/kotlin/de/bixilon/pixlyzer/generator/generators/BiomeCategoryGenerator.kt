package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass

object BiomeCategoryGenerator : Generator(
    "biome_categories",
    allowEmpty = true,
) {
    private val CATEGORY_CLASS = getClass("net.minecraft.world.biome.Biome\$Category")

    override fun generate() {
        val categories = CATEGORY_CLASS?.enumConstants ?: return
        for (biomeCategory in categories) {
            check(biomeCategory is Enum<*>)
            data[biomeCategory.ordinal] = mutableMapOf("name" to biomeCategory.name)
        }
    }

}
