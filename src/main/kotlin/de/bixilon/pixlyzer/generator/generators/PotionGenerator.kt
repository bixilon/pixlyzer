package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util.compound
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.potion.Potion

object PotionGenerator : Generator(
    "potions"
) {
    val REGISTRY = RegistryUtil.getRegistry<Potion>("POTION")!!

    override fun generate() {
        for (potion in REGISTRY) {
            val resourceLocation = REGISTRY.getId(potion)
            val potionData = compound()

            potionData["id"] = REGISTRY.getRawId(potion)

            (POTION_NAME_FIELD.get(potion) as String?)?.let {
                potionData["name"] = it
            }

            val effects = mutableListOf<Any>()

            for (effect in potion.getEffects()) {
                effects += effect.serialize()
            }
            if (effects.size > 0) {
                potionData["effects"] = effects
            }

            data[resourceLocation.toString()] = potionData
        }
    }

    val POTION_NAME_FIELD = getField(Potion::class.java, "baseName", "name")!!


    private fun StatusEffectInstance.serialize(): MutableMap<Any, Any> {
        val mobEffect = compound()

        effectType.let {
            mobEffect["effect"] = StatusEffectGenerator.REGISTRY.getId(it).toString()
        }

        mobEffect["duration"] = duration
        mobEffect["amplifier"] = amplifier
        mobEffect["ambient"] = isAmbient
        mobEffect["visible"] = shouldShowParticles()
        mobEffect["showIcon"] = shouldShowIcon()



        return mobEffect
    }
}

