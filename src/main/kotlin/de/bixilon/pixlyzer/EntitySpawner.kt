package de.bixilon.pixlyzer

import com.mojang.authlib.GameProfile
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.ReflectionUtil.setFinalField
import net.minecraft.client.network.ClientPlayNetworkHandler
import net.minecraft.client.network.OtherClientPlayerEntity
import net.minecraft.client.world.ClientWorld
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityType
import net.minecraft.entity.boss.WitherEntity
import net.minecraft.entity.decoration.ItemFrameEntity
import net.minecraft.recipe.RecipeManager
import net.minecraft.scoreboard.Scoreboard
import net.minecraft.world.World
import net.minecraft.world.border.WorldBorder
import net.minecraft.world.dimension.DimensionType
import org.objenesis.Objenesis
import org.objenesis.ObjenesisStd
import java.util.*


object EntitySpawner {
    private val EGG_ENTITY_CLASS = getClass("net.minecraft.entity.thrown.ThrownEggEntity", "net.minecraft.entity.projectile.thrown.EggEntity")!!
    private val SNOWBAL_ENTITY_CLASS = getClass("net.minecraft.entity.thrown.SnowballEntity", "net.minecraft.entity.projectile.thrown.SnowballEntity")!!
    private val ENDER_PEARL_ENTITY_CLASS = getClass("net.minecraft.entity.thrown.ThrownEnderpearlEntity", "net.minecraft.entity.projectile.thrown.EnderPearlEntity")!!
    private val EYE_OF_ENDER_ENTITY_CLASS = getClass("net.minecraft.entity.EnderEyeEntity", "net.minecraft.entity.EyeOfEnderEntity", "net.minecraft.entity.projectile.thrown.EyeOfEnderEntity")!!
    private val EXPERIENCE_BOTTLE_ENTITY_CLASS = getClass("net.minecraft.entity.thrown.ThrownExperienceBottleEntity", "net.minecraft.entity.projectile.thrown.ExperienceBottleEntity")!!
    private val POTION_ENTITY_ENTITY_CLASS = getClass("net.minecraft.entity.thrown.ThrownPotionEntity", "net.minecraft.entity.projectile.thrown.PotionEntity")!!


    private val ATOMIC_RANDOM_CLASS = getClass("net.minecraft.util.math.random.CheckedRandom", "net.minecraft.util.math.random.AtomicSimpleRandom", "net.minecraft.world.gen.random.AtomicSimpleRandom")

    fun summonEntity(entityType: EntityType<*>): Entity {
        when (entityType) {
            EntityType.PLAYER -> {
                return try {
                    OtherClientPlayerEntity::class.java.getConstructor(levelClass, GameProfile::class.java, getClass("net.minecraft.network.encryption.PlayerPublicKey")).newInstance(CLIENT_LEVEL, GameProfile(UUID.randomUUID(), "dummy"), null)
                } catch (exception: NoSuchMethodException) {
                    OtherClientPlayerEntity::class.java.getConstructor(levelClass, GameProfile::class.java).newInstance(CLIENT_LEVEL, GameProfile(UUID.randomUUID(), "dummy"))
                }
            }
            EntityType.LIGHTNING_BOLT -> return OBJENSIS.newInstance(LIGHTNING_BOLT_CLASS) as Entity
            EntityType.FISHING_BOBBER -> return OBJENSIS.newInstance(FISHING_HOOK_CLASS) as Entity
            EntityType.ITEM_FRAME -> return OBJENSIS.newInstance(ItemFrameEntity::class.java) as Entity
        }


        val entity = try {
            ENTITY_CREATE_METHOD?.invoke(FACTORY_FIELD.get(entityType), entityType, CLIENT_LEVEL) as Entity?
        } catch (exception: Throwable) {
            exception.printStackTrace()
            null // ToDo
        }


        if (entity != null) {
            return entity
        }

        // ToDo: This crashes in 21w13a, is an issue in tiny remapper, should be fixed by now
        when (entityType) {
            EntityType.EGG -> return OBJENSIS.newInstance(EGG_ENTITY_CLASS) as Entity
            EntityType.SNOWBALL -> return OBJENSIS.newInstance(SNOWBAL_ENTITY_CLASS) as Entity
            EntityType.ENDER_PEARL -> return OBJENSIS.newInstance(ENDER_PEARL_ENTITY_CLASS) as Entity
            EntityType.EYE_OF_ENDER -> return OBJENSIS.newInstance(EYE_OF_ENDER_ENTITY_CLASS) as Entity
            EntityType.EXPERIENCE_BOTTLE -> return OBJENSIS.newInstance(EXPERIENCE_BOTTLE_ENTITY_CLASS) as Entity
            EntityType.POTION -> return OBJENSIS.newInstance(POTION_ENTITY_ENTITY_CLASS) as Entity
            EntityType.WITHER -> return OBJENSIS.newInstance(WitherEntity::class.java) as Entity
        }

        TODO("Entity type: $entityType")
    }


    private val LIGHTNING_BOLT_CLASS = getClass("net.minecraft.entity.LightningEntity")!!
    private val FISHING_HOOK_CLASS = getClass("net.minecraft.entity.FishingBobberEntity", "net.minecraft.entity.projectile.FishingBobberEntity")!!

    private val levelClass = getClass("net.minecraft.client.world.ClientWorld")

    private val FACTORY_FIELD = getField(EntityType::class.java, "factory")!!

    private val ENTITY_CREATE_METHOD = try {
        getClass("net.minecraft.entity.EntityType\$EntityFactory")?.getDeclaredMethod("create", EntityType::class.java, World::class.java)
    } catch (exception: Exception) {
        null
    }

    var OBJENSIS: Objenesis = ObjenesisStd()

    val CLIENT_LEVEL = OBJENSIS.newInstance(levelClass) as ClientWorld

    fun World.setDimension() {
        getClass("net.minecraft.world.dimension.OverworldDimension", "net.minecraft.world.dimension.DimensionType")?.let {
            val dimension = OBJENSIS.newInstance(it)
            getField(DimensionType::class.java, "minimumY")?.setInt(dimension, 0)
            getField(DimensionType::class.java, "height")?.setInt(dimension, 256)
            getField(DimensionType::class.java, "hasSkyLight")?.setBoolean(dimension, false)
            getField(World::class.java, "dimensionEntry")?.let {
                setFinalField(getField(World::class.java, "dimensionEntry")!!, this, getClass("net.minecraft.registry.entry.RegistryEntry\$Direct", "net.minecraft.util.registry.RegistryEntry\$Direct")!!.getConstructor(Any::class.java).newInstance(dimension))
            }

            try {
                setFinalField(getField(World::class.java, "dimension")!!, this, dimension)
            } catch (ignored: Throwable) {
            }
        }
    }


    init {
        val randomField = getField(World::class.java, "random")!!
        setFinalField(randomField, CLIENT_LEVEL, if (randomField.type == Random::class.java) Random() else ATOMIC_RANDOM_CLASS!!.getConstructor(Long::class.java).newInstance(0L))
        setFinalField(getField(World::class.java, "border")!!, CLIENT_LEVEL, WorldBorder())
        setFinalField(getField(World::class.java, "properties")!!, CLIENT_LEVEL, OBJENSIS.newInstance(getClass("net.minecraft.world.level.LevelProperties")))
        setFinalField(getField(levelClass, "scoreboard")!!, CLIENT_LEVEL, Scoreboard())
        CLIENT_LEVEL.setDimension()

        val networkHandler = OBJENSIS.newInstance(ClientPlayNetworkHandler::class.java)
        networkHandler.recipeManager = RecipeManager()
        getField(ClientWorld::class.java, "networkHandler", "netHandler")!!.set(CLIENT_LEVEL, networkHandler)
    }

}
