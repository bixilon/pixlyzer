package de.bixilon.pixlyzer.util

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.type.MapType
import net.minecraft.client.MinecraftClient
import java.io.*
import java.security.MessageDigest


object Util {
    val MAPPER = ObjectMapper()
        .setSerializationInclusion(JsonInclude.Include.NON_NULL)

    init {
        MAPPER.setDefaultPrettyPrinter(PrettyPrinter())
    }

    val JSON_MAP_TYPE: MapType = MAPPER.typeFactory.constructMapType(HashMap::class.java, Any::class.java, Any::class.java)


    fun <T> forceClassInit(clazz: Class<T>) {
        try {
            Class.forName(clazz.name, true, clazz.classLoader)
        } catch (exception: ClassNotFoundException) {
            throw RuntimeException(exception)
        }
    }

    fun readJsonResource(path: String): MutableMap<String, Any> {
        return MAPPER.readValue(readReader(BufferedReader(InputStreamReader(Util::class.java.getResourceAsStream("/$path")!!))), JSON_MAP_TYPE)!!
    }

    fun readJsonFile(path: String): MutableMap<String, Any> {
        return MAPPER.readValue(readReader(BufferedReader(InputStreamReader(FileInputStream(path)))), JSON_MAP_TYPE)!!
    }

    fun byteArrayToHexString(byteArray: ByteArray): String {
        val result = StringBuilder()
        for (value in byteArray) {
            result.append(((value.toInt() and 0xff) + 0x100).toString(16).substring(1))
        }
        return result.toString()
    }

    private fun readReader(reader: BufferedReader): String {
        val stringBuilder = StringBuilder()
        var line: String?
        while (reader.readLine().also { line = it } != null) {
            stringBuilder.append(line)
            stringBuilder.append('\n')
        }
        stringBuilder.deleteCharAt(stringBuilder.length - 1)
        reader.close()
        return stringBuilder.toString()
    }


    fun readJsonMinecraftResource(path: String): MutableMap<String, Any> {
        val inputStream = MinecraftClient::class.java.getResourceAsStream("/$path") ?: throw FileNotFoundException("Can not find minecraft resource: $path")
        return MAPPER.readValue(readReader(BufferedReader(InputStreamReader(inputStream))), JSON_MAP_TYPE)!!
    }

    inline fun <reified T> Any.nullCast(): T? {
        if (this is T) {
            return this
        }
        return null
    }

    val Class<*>.realName: String
        get() = this.name.removePrefix(this.packageName).removePrefix(".")

    fun Class<*>.of(name: String): Boolean {
        var `class` = this
        while (`class` != Object::class.java) {
            if (`class`.realName == name) {
                return true
            }

            `class` = `class`.superclass
        }
        return false
    }


    fun compound(vararg pairs: Pair<Any, Any>): MutableMap<Any, Any> {
        return mutableMapOf(*pairs)
    }

    fun Any.mapCast(): MutableMap<Any, Any>? {
        try {
            return this as MutableMap<Any, Any>
        } catch (ignored: ClassCastException) {
        }
        return null
    }

    fun <T> Any.listCast(): MutableList<T>? {
        try {
            return this as MutableList<T>
        } catch (ignored: ClassCastException) {
        }
        return null
    }

    fun saveHashFile(stream: InputStream, tempPath: String, outputStream: OutputStream, outputPath: String): String {

        val buffer = ByteArray(4096)
        var length: Int
        while (stream.read(buffer, 0, buffer.size).also { length = it } != -1) {
            outputStream.write(buffer, 0, length)
        }
        outputStream.close()


        val fileInputStream = FileInputStream(tempPath)

        val crypt = MessageDigest.getInstance("SHA-1")

        while (fileInputStream.read(buffer, 0, buffer.size).also { length = it } != -1) {
            crypt.update(buffer, 0, length)
        }
        fileInputStream.close()

        val hash: String = byteArrayToHexString(crypt.digest())

        val outputFile = File(outputPath.replace("\${shortHash}", hash.substring(0, 2)).replace("\${hash}", hash))
        outputFile.parentFile.mkdirs()

        if (outputFile.exists()) {
            println("Hash file does already exist, skipping")
        } else {
            File(tempPath).renameTo(outputFile)
            println("Generated and saved hash file to ${outputFile.absolutePath}")
        }

        return hash
    }
}
