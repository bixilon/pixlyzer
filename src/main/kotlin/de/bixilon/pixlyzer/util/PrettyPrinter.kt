package de.bixilon.pixlyzer.util

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.util.DefaultIndenter
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter


class PrettyPrinter : DefaultPrettyPrinter {

    constructor() {
        _arrayIndenter = DefaultIndenter.SYSTEM_LINEFEED_INSTANCE
        _objectIndenter = DefaultIndenter.SYSTEM_LINEFEED_INSTANCE
    }

    constructor(base: DefaultPrettyPrinter) : super(base)


    override fun writeObjectFieldValueSeparator(generator: JsonGenerator) {
        generator.writeRaw(": ")
    }

    override fun createInstance(): PrettyPrinter {
        check(javaClass == PrettyPrinter::class.java) {
            ("Failed `createInstance()`: " + javaClass.name
                    + " does not override method; it has to")
        }
        return PrettyPrinter(this)
    }
}
