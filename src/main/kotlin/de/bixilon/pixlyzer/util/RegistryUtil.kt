package de.bixilon.pixlyzer.util

import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import net.minecraft.util.Identifier

object RegistryUtil {
    val REGISTRIES = ReflectionUtil.getClass("net.minecraft.registry.Registries", "net.minecraft.util.registry.Registries")
    private val BUILTIN_REGISTRIES = ReflectionUtil.getClass("net.minecraft.registry.BuiltinRegistries", "net.minecraft.util.registry.BuiltinRegistries", "net.minecraft.data.BuiltinRegistries")
    val REGISTRY = ReflectionUtil.getClass("net.minecraft.registry.Registry", "net.minecraft.util.registry.Registry")!!
    private val GET_ID = REGISTRY.getDeclaredMethod("getId", Any::class.java)
    private val GET_RAW_ID = REGISTRY.getDeclaredMethod("getRawId", Any::class.java)
    private val GET = REGISTRY.getDeclaredMethod("get", Identifier::class.java)
    private val ITERATOR = Iterable::class.java.getDeclaredMethod("iterator")

    fun <T> getRegistry(vararg names: String): PixLyzerRegistry<T>? {
        return _getRegistry(*names)?.let { PixLyzerRegistry(it) }
    }

    fun _getRegistry(vararg names: String): Any? {
        getField(BUILTIN_REGISTRIES, *names)?.get(null)?.let { return it }
        getField(REGISTRIES, *names)?.get(null)?.let { return it }

        return getField(REGISTRY, *names)?.get(null)
    }

    class PixLyzerRegistry<T>(
        val native: Any?,
    ) : Iterable<T> {
        fun getId(item: T?): Identifier? {
            return GET_ID.invoke(native, item).unsafeCast()
        }

        fun getRawId(item: T?): Int {
            return GET_RAW_ID.invoke(native, item).unsafeCast()
        }

        fun get(identifier: Identifier): T {
            return GET.invoke(native, identifier).unsafeCast()
        }

        override fun iterator(): Iterator<T> {
            return ITERATOR.invoke(native).unsafeCast()
        }
    }
}
