package de.bixilon.pixlyzer.util

import org.apache.commons.lang3.reflect.FieldUtils
import java.lang.reflect.Field
import java.lang.reflect.Method

object ReflectionUtil {

    fun getClass(vararg names: String): Class<Any>? {
        for (name in names) {
            try {
                return Class.forName(name) as Class<Any>
            } catch (exception: ClassNotFoundException) {
            }
        }
        return null
    }

    fun getField(clazz: Class<*>?, vararg names: String): Field? {
        if (clazz == null) {
            return null
        }
        for (name in names) {
            try {
                val field = clazz.getField(name)
                field.isAccessible = true
                return field
            } catch (exception: NoSuchFieldException) {
                continue
            }
        }
        return null
    }

    @JvmName(name = "getGetter2")
    fun Class<*>.getGetter(vararg methodNames: String): Method? {
        return getGetter(this, *methodNames)
    }

    fun getGetter(clazz: Class<*>?, vararg methodNames: String): Method? {
        if (clazz == null) {
            return null
        }
        for (name in methodNames) {
            try {
                val method = clazz.getMethod(name)
                method.isAccessible = true
                return method
            } catch (exception: NoSuchMethodException) {
                continue
            }
        }
        return null
    }

    fun setFinalField(field: Field, instance: Any, value: Any) {
        field.isAccessible = true

        FieldUtils.removeFinalModifier(field)

        field.set(instance, value)
    }

    fun <T> Any.invoke(name: String, types: Array<Class<*>> = arrayOf(), parameters: Array<Any> = arrayOf()): T? {
        val clazz = if (this is Class<*>) this else this::class.java
        val method = try {
            clazz.getMethod(name, *types)
        } catch (exception: NoSuchMethodException) {
            return null
        }
        return method.invoke(this, *parameters) as T
    }

    fun <T> Any.variable(vararg names: String): T? {
        return getField(this::class.java, *names)?.get(this) as T?
    }

    fun <T> Any.invokeGetter(vararg names: String): T? {
        return getGetter(this::class.java, *names)?.invoke(this) as T?
    }
}
