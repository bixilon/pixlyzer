package de.bixilon.pixlyzer

import de.bixilon.kutil.reflection.ReflectionUtil.forceInit
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.MBFCompressionTypes
import de.bixilon.mbf.MBFData
import de.bixilon.mbf.MBFDataInfo
import de.bixilon.pixlyzer.exceptions.GeneratorSkipException
import de.bixilon.pixlyzer.generator.Generators
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.ReflectionUtil.getGetter
import de.bixilon.pixlyzer.util.ReflectionUtil.invoke
import de.bixilon.pixlyzer.util.ReflectionUtil.variable
import de.bixilon.pixlyzer.util.RegistryUtil
import de.bixilon.pixlyzer.util.Util
import net.minecraft.Bootstrap
import java.io.*
import java.lang.reflect.Field
import java.util.*
import java.util.zip.GZIPOutputStream

object PixLyzer {
    private val START_TIME = System.currentTimeMillis()
    val ENTITY_DATA_MAPPING = Util.readJsonResource("entities_data_mappings.json")
    val TAG_CLASS = getClass("net.minecraft.tag.Tag", "com.mojang.datafixers.types.templates.Tag")

    val MODULES = listOf(
        "de.bixilon.pixlyzer.physics.PhysicsExtractor",
    )

    lateinit var outputDirectory: File


    private fun initializeTags(tags: Field) {
        val list = tags.get(null)?.variable<Iterable<Any>>("tags") ?: return
        for (tag in list) {
            getField(tag::class.java, "delegate")!!.set(tag, TAG_CLASS!!.invoke("of", arrayOf(Set::class.java), arrayOf(setOf<Any>())))
        }
    }

    fun initializeTags(vararg name: String) {
        val clazz = getClass(*name)
        getField(clazz, "REQUIRED_TAGS")?.let { initializeTags(it) }
    }

    @JvmStatic
    fun main(args: Array<String>) {
        println("Starting PixLyzer")

        if (args.size != 5) {
            error("Usage: java -cp minecraft,pixlyzer de.bixilon.pixlyzer.PixLyzer <Output directory> <Hash directory> <Json assets index> <MBF assets index> <Version id>")
        }

        outputDirectory = File(args[0])
        outputDirectory.mkdirs()
        val hashDirectory = File(args[1])
        hashDirectory.mkdirs()
        val jsonAssetsIndex = File(args[2])
        val mbfAssetsIndex = File(args[3])
        val versionId = args[4]


        try {
            Class.forName("net.minecraft.DetectedVersion")
            throw IllegalStateException("This branch only works with yarn mappings, not with mojangs!")
        } catch (exception: ClassNotFoundException) {
        }

        try {
            Class.forName("net.minecraft.MinecraftVersion")
        } catch (exception: ClassNotFoundException) {
            throw IllegalStateException("Can not find minecraft in the classpath. Please add it to the classpath and restart PixLyzer", exception)
        }



        println("Loading classes...")
        val classesLoadStartTime = System.currentTimeMillis()

        initializeGameVersionMethod?.invoke(null)

        Bootstrap.initialize()

        Util.forceClassInit(RegistryUtil.REGISTRY)
        RegistryUtil.REGISTRIES?.forceInit()


        initializeTags("net.minecraft.registry.tag.FluidTags", "net.minecraft.tag.FluidTags")
        initializeTags("net.minecraft.registry.tag.ItemTags", "net.minecraft.tag.ItemTags")
        initializeTags("net.minecraft.registry.tag.BlockTags", "net.minecraft.tag.BlockTags")
        initializeTags("net.minecraft.registry.tag.GameEventTags", "net.minecraft.tag.GameEventTags")
        initializeTags("net.minecraft.registry.tag.EntityTypeTags", "net.minecraft.tag.EntityTypeTags")

        println("Class loading done in ${System.currentTimeMillis() - classesLoadStartTime}ms")


        for (module in MODULES) {
            val clazz = getClass(module)
            if (clazz == null) {
                println("Can not find module: $module")
                continue
            }
            try {
                clazz.forceInit()
                clazz.getDeclaredMethod("start").invoke(null)
            } catch (error: Throwable) {
                println("Can not execute module: $error")
                error.printStackTrace()
            }
        }

        val all: MutableMap<String, Any> = mutableMapOf()
        for (generator in Generators.GENERATORS) {
            val startTime = System.currentTimeMillis()
            println("Starting ${generator.name}...")

            try {
                generator.generate()
            } catch (exception: GeneratorSkipException) {
                // not implemented
                println("Skipping ${generator.name}: ${exception.message}")
                continue
            } catch (exception: Throwable) {
                if (generator.allowedFail) {
                    println("Generator ${generator.name} crashed: $exception")
                    continue
                }
                throw exception
            }
            if (generator.entries == 0) {
                if (generator.allowEmpty) {
                    println("Generator ${generator.name} has 0 entries!")
                    continue
                } else {
                    error("${generator.name} has 0 entries!")
                }
            } else {
                generator.test()
                println("Tests succeeded for ${generator.name}")
            }

            println("Saving to ${outputDirectory.absolutePath}/${generator.name}.json")

            writData("${outputDirectory.absolutePath}/${generator.name}", generator.data)
            all[generator.name] = generator.data


            println("Done generating ${generator.name} in ${System.currentTimeMillis() - startTime}ms, generated ${generator.entries} entries.")
        }
        println("Eventually writing all to file...")

        val tempFileOutputPath = System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID().toString()

        if (File(jsonAssetsIndex.absolutePath + ".json").exists()) {

            writData("${outputDirectory.absolutePath}/all", all)

            val jsonHash = Util.saveHashFile(all.toJsonString().byteInputStream(), tempFileOutputPath, GZIPOutputStream(FileOutputStream(tempFileOutputPath)), hashDirectory.absolutePath + "/\${shortHash}/\${hash}.gz")

            val assetsIndexJson = Util.readJsonFile(jsonAssetsIndex.absolutePath + ".json")

            assetsIndexJson[versionId] = jsonHash

            writData(jsonAssetsIndex.absolutePath, assetsIndexJson)
        } else {
            println("Can not find json index file! Not indexing it.")
        }

        if (File(mbfAssetsIndex.absolutePath + ".json").exists()) {
            val mbfStream = ByteArrayOutputStream()
            MBFBinaryWriter(mbfStream).writeMBF(MBFData(dataInfo = MBFDataInfo(compression = MBFCompressionTypes.ZSTD, preferVariableTypes = true, variableLengthPrefix = true, compressionLevel = 22), data = all))

            val mbfHash = Util.saveHashFile(ByteArrayInputStream(mbfStream.toByteArray()), tempFileOutputPath, FileOutputStream(tempFileOutputPath), hashDirectory.absolutePath + "/\${shortHash}/\${hash}.mbf")


            val assetsIndexData = Util.readJsonFile(mbfAssetsIndex.absolutePath + ".json")

            assetsIndexData[versionId] = mbfHash

            writData(mbfAssetsIndex.absolutePath, assetsIndexData)
        } else {
            println("Can not find mbf index file! Not indexing it.")
        }

        println("Done in ${System.currentTimeMillis() - START_TIME}ms")
    }

    fun Any.toJsonString(): String {
        return Util.MAPPER.writeValueAsString(this)
    }

    fun writData(path: String, data: Any) {
        val file = File(path)
        file.parentFile.mkdirs()
        run {
            val fileWriter = FileWriter("$path.json")
            fileWriter.write(Util.MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(data))
            fileWriter.close()
        }
        run {
            val fileWriter = FileWriter("$path.min.json")
            fileWriter.write(Util.MAPPER.writeValueAsString(data))
            fileWriter.close()
        }

        ByteArrayOutputStream().let {
            MBFBinaryWriter(it).writeMBF(MBFData(dataInfo = MBFDataInfo(compression = MBFCompressionTypes.ZSTD, preferVariableTypes = true, variableLengthPrefix = true, compressionLevel = 22), data = data))
            val fileWriter = FileOutputStream("$path.mbf")
            fileWriter.write(it.toByteArray())
            fileWriter.close()
        }
    }


    private val initializeGameVersionMethod = getGetter(getClass("net.minecraft.SharedConstants"), "createGameVersion", "method_36208")
}
