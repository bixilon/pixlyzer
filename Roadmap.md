# Roadmap

## Features implemented

- Entity data
  - Entity id, sizes, health, modifiers
  - Identifier, Inherits
  - Meta data
- Items
- Dimensions
- Blocks
- Biomes
- Particles
- Statistics
- General version information
  - Version name
  - Protocol version
  - Data version
- Block entities
- Sounds
  - Categories
  - Sounds

## More features (I could implement)

- Entity data
  - Models
  - Entity animations
- Recipes
- Packets
- Block actions
- Achievements
